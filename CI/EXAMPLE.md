# Exemple : Produire, publier, utiliser images Docker

Sous Forge MIA - cas projet public wcsp
=======================================

Build and Publish
-----------------

 - CI/docker/Dockerfile file

 - .gitlab-ci.yml :

   Verifies CI/docker/Dockerfile file and builds/publishes images

 => Container Registry :

     registry.forgemia.inra.fr/nathalie.rousse/wcsp/wcsp_py_toulbar2:b89bffcfefca3862a61af4064601a117e078d020

     registry.forgemia.inra.fr/nathalie.rousse/wcsp/wcsp_py_toulbar2:latest

Use
---

  Voir CI/docker_use

Sous Forge MIA - cas projet prive sandbox
=========================================

Voir sandbox/README_CI.md

Sous GitHub
===========

Voir nrousse/toulbar2 : .github/workflows (docker-publish.yml...)

Docker and GitHub CI
--------------------

- By GitHub Actions :

  - Docker image :
    Build a Docker image to deploy, run, or push to a registry. 

  - Publish Docker Container :
    Build, test and push Docker image to GitHub Packages. 

- Publishing Docker images :
*[Source https://docs.github.com/en/actions/guides/publishing-docker-images]*

  You can publish Docker images to a registry, such as **Docker Hub** or 
  **GitHub Packages**, as part of your continuous integration (CI) workflow.


