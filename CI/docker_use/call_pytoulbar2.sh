#!/bin/bash

###############################################################################
# script to launch "python3 call.py" command in Docker image
###############################################################################

source /WORK.sh

export PATH

PYTHONPATH=$PYTHONPATH:/WORK:/WORK/pythonplus
export PYTHONPATH

# specific required
pip3 install --cache-dir /WORK/cache -t /WORK/pythonplus --upgrade numpy

# run
cd /WORK
python3 problem.py

# clean
rm -fr /WORK/pythonplus
rm -fr /WORK/cache

