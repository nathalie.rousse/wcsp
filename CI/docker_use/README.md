###############################################################################
# 
#              Using Docker image of (py)toulbar2
# 
###############################################################################

# Soit appel de l'image locale wcsp_py_toulbar2 qui doit avoir ete installee
# au prealable, ainsi :

  docker pull registry.forgemia.inra.fr/nathalie.rousse/wcsp/wcsp_py_toulbar2:latest
  docker tag registry.forgemia.inra.fr/nathalie.rousse/wcsp/wcsp_py_toulbar2:latest wcsp_py_toulbar2:latest
  docker rmi registry.forgemia.inra.fr/nathalie.rousse/wcsp/wcsp_py_toulbar2:latest 

# Soit appel de l'image distante
# registry.forgemia.inra.fr/nathalie.rousse/wcsp/wcsp_py_toulbar2:latest :

  Dans ce cas, remplacer ci-dessous wcsp_py_toulbar2 par
  registry.forgemia.inra.fr/nathalie.rousse/wcsp/wcsp_py_toulbar2:latest


# Call toulbar2 ---------------------------------------------------------------
 
  - call_toulbar2.sh : script to launch commands
 
  - command :
    docker run -v $PWD:/WORK -ti wcsp_py_toulbar2 /bin/bash /WORK/call_toulbar2.sh
 
# Call pytoulbar2 -------------------------------------------------------------
 
  - problem.py : python code using pytoulbar2
 
  - call_pytoulbar2.sh : script to launch "python problem.py" 
 
  - command :
    docker run -v $PWD:/WORK -ti wcsp_py_toulbar2 /bin/bash /WORK/call_pytoulbar2.sh
 
###############################################################################

