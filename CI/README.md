# Continuous Integration
  
General
=======

*[Source : https://www.beopenit.com/github-actions-le-nouveau-pipeline-ci-cd-de-github ]*

  Il y a deux façons d’implémenter des fonctionnalités CI/CD :

  - Soit utiliser un serveur CI/CD tiers (Jenkins...) :

    Offre le plus de flexibilité et de portabilité.

    Demande le plus de ressources et de compétences techniques. 

    Exemples : Jenkins, Bamboo, CircleCI, Aws codeDeploy etc.

  - Soit utiliser un serveur CI/CD embarqué par le serveur de versionning :

    Plus facile à utiliser et demande moins de compétences techniques.

    Moins portable (lié au serveur de versionning).

    Exemples : GitHub Actions, Gitlab-CI, Bitbucket pipeline etc.


CI/CD files
===========

Files where CI/CD jobs are defined :

  - GitHub case :

    - Serveur CI/CD tiers : .travis.yml ...

    - GitHub Actions : .github/workflows/ * .yml (docker-publish.yml ...)

  - Forge MIA case : .gitlab-ci.yml


GitHub Actions, le serveur CI/CD embarqué de GitHub
===================================================

*[Source : https://docs.github.com/en/actions/guides/about-continuous-integration ]*

  You can create custom continuous integration (CI) and continuous deployment
  (CD) workflows directly in your GitHub repository with GitHub Actions.

- Tarif :

*[Source : https://docs.github.com/en/billing/managing-billing-for-github-actions/managing-your-spending-limit-for-github-actions#about-spending-limits-for-github-actions ]*

  GitHub Actions usage is free for both public repositories and self-hosted
  runners.

  For private repositories, each GitHub account receives a certain amount of
  free minutes and storage, depending on the product used with the account.
  Any usage beyond the included amounts is controlled by spending limits.

- GitHub-hosted runners :

*[Source : https://docs.github.com/en/actions/using-github-hosted-runners/about-github-hosted-runners ]*

  GitHub offers hosted virtual machines to run workflows. The virtual machine
  contains an environment of tools, packages, and settings available for
  GitHub Actions to use.

  A GitHub-hosted runner is a virtual machine hosted by GitHub with the GitHub
  Actions runner application installed. GitHub offers runners with Linux,
  Windows, and macOS operating systems.

- Liens :

  GitHub Actions guides : https://docs.github.com/en/actions/guides

  https://docs.github.com/en/actions/learn-github-actions

Forge MIA
=========

  https://forgemia.inra.fr/help

  https://forgemia.inra.fr/help/ci/quick_start/index.md

  https://forgemia.inra.fr/help/ci/yaml/README.md

  https://forgemia.inra.fr/help/ci/examples/README.md


Exemple : Produire, publier, utiliser images Docker
===================================================

Voir EXAMPLE.md

