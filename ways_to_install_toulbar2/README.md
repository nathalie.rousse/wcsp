# Install/use toulbar2
  
A. Different ways to install/use toulbar2
=========================================

- installation locale
- installation sur serveur tapou
- travail dans une machine virtuelle
- travail dans un container (docker, singularity)
- appel de services web

A.a. Installation locale
========================

- Appel depuis : poste local

- Description :
  - Installation de (py)toulbar2 sur poste local,
    voir "procedure build (py)toulbar2"

- Use :
  - see ways_to_call_toulbar2/README.md :
    "Call by python code", "Call by C++ code"

A.b. Installation sur serveur tapou
===================================

- Appel depuis : serveur tapou, poste local par ssh

- Having an account on tapou server. Si a distance, acces par VPN.

- Description :
  - connect :
    ssh username@tapou.toulouse.inra.fr
  - Installation de (py)toulbar2 sur serveur tapou,
    voir "procedure build (py)toulbar2"

- Use :
  - connect :
    ssh username@tapou.toulouse.inra.fr
  - see ways_to_call_toulbar2/README.md :
    "Call by python code", "Call by C++ code"

A.c. Travail dans une machine virtuelle
=======================================

- Appel depuis : poste local

- Description :
  - machine virtuelle produite et deposee sur poste local
  - avoir installe sur poste local outil de virtualisation (VirtualBox...)
    permettant de demarrer la machine virtuelle.

- Use : 
  - commands into the virtual machine :
    see ways_to_call_toulbar2/README.md :
    "Call by python code", "Call by C++ code"

- _todo_ Existing virtual machines, known problems...

A.d. Travail dans un container (docker, singularity)
====================================================

- Appel depuis : poste local

- Description :
  - avoir installe sur poste local le logiciel/systeme de containerisation
    (Docker, Singularity...).

  - Docker case :
    - le fichier recette Dockerfile ecrit et depose sur poste local. 
      ...voir ws_deliv/toulbar2,pytoulbar2 : Dockerfile file
    - build : 
      ...voir ws_deliv/toulbar2,pytoulbar2 : the build command

  - Singularity case :

    - Le container Singularity peut etre construit a partir d'un fichier 
     recette Singularity filename.def (ecrit et depose sur poste local) ou
     alors a partir d'un container Docker (pre-existant sur poste local).

    - build :
      ...voir ws_deliv/toulbar2,pytoulbar2 : the build command

- Use :

  - Docker case :
    _todo_ ...voir ws_deliv/toulbar2,pytoulbar2 : exemples
  
  - Singularity case :
    _todo_ ...voir ws_deliv/toulbar2,pytoulbar2 : exemples

A.e. Appel de services web
==========================

- Appel depuis : poste local

- Description :

  - disposer sur poste local de l'outil/logiciel qui enverra/recevra
    http requests/responses (curl, web browser, langage C++, python, R...).

  - les services web (d'appel de toulbar2) doivent exister, mis en production
    sur un serveur.

- Use :
  - see ways_to_call_toulbar2/README.md :
    "Call by http requests/responses"

Procedure : how to build (py)toulbar2
=====================================

Voir build_commands.txt

