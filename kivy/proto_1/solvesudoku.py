""" Solve sudoku App """

MODE = "WS" # MODE values : "WS", "LOCAL"
DEBUG = True

import kivy
#kivy.require('1.9.0') # ???

#from kivy.logger import Logger
#import logging
#Logger.setLevel(logging.TRACE)

from kivy.app import App
from kivy.config import Config
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.settings import SettingsWithTabbedPanel
from kivy.properties import StringProperty
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.button import Button

import time, os

if MODE=="LOCAL":
    from visualsudoku.toulbar2_visual_sudoku_puzzle import read_and_solve
elif MODE=="WS":
    from ws.ws import read_and_solve

HOME_PATH = "/home/nrousse/workspace_git/wcsp/kivy/proto_1"
CONFIG_FILE_PATH = os.path.join(HOME_PATH, "solvesudoku.ini")
IMG_PATH = os.path.join(HOME_PATH, "img")

INI = {
        'keep_default': 0,
        'border_default': 0,
        'time_default': 5,
        'model':"visualsudoku/mixed_classifier.h5",
        'debug' : 0
      }

SETTINGS = {}

json_data_app = \
"""[
      {"type": "bool",
       "title": "expert",
       "desc": "Expert Mode to access some more settings...",
       "section": "app", "key": "expert"},

      {"type": "path",
       "title": "imagepath",
       "desc": "Image path where images are saved",
       "section": "app", "key": "imagepath"},

      {"type": "bool",
       "title": "savingoutputfile",
       "desc": "Saving solution file",
       "section": "app", "key": "savingoutputfile"},

      {"type": "bool",
       "title": "savinginputfile",
       "desc": "Saving captured partial grid file (from camera)",
       "section": "app", "key": "savinginputfile"}
]"""

def get_config_file_name():
    return CONFIG_FILE_PATH

def is_a_file(filepath):
    if os.path.exists(filepath):
        if os.path.isfile(filepath):
            return True
    return False

def error_msg(text):
    layout = GridLayout(cols=1, padding=10)
    popup_label1  = Label(text=text)
    close_button = Button(text = "Close")
    layout.add_widget(popup_label1)
    layout.add_widget(close_button)       
    popup = Popup(title='ERROR', content=layout, size_hint=(0.5, 0.5))
    popup.open()   
    close_button.bind(on_press=popup.dismiss)   

def failed_msg(exception) :
    errortype = type(exception).__name__
    errordetails = exception.args
    error_text = "Error " + errortype + ": "
    for m in errordetails :
        error_text = error_text + str(m) + " -- "
    if DEBUG :
        print("[FAILED ] ERROR", errortype, ":", exception)
    error_msg(text=error_text)

#------------------------------------------------------------------------------
# Screens
#------------------------------------------------------------------------------

class MainScreen(Screen):
    pass

class SetScreen(Screen):
    pass

class SelectImageFileScreen(Screen):
    """Selection of the image file (existing on device) to be solved"""

    def select_file(self, *args):
        sm = self.manager
        screen = sm.screens[sm.number['displayimage']]
        screen.ids.imagepath.text = self.ids.fc.selection[0]
        sm.current = 'displayimage'

class DisplayImageScreen(Screen):
    """Display the chosen image file (existing or captured) to be solved

    Buttons : solve, back to
    """

    def getname_outputfilepath(self, inputfilepath=None):
        
        dirname = SETTINGS["imagepath"]
        out_name = "solution.jpg" # default"
        if inputfilepath is not None :
            #dirname = os.path.dirname(inputfilepath)
            timestr = time.strftime("%Y%m%d_%H%M%S")
            in_name = os.path.basename(inputfilepath)
            out_name = "SOL_{}_{}".format(timestr, in_name)
        outputfilepath = os.path.join(dirname, out_name)
        return outputfilepath

    def solve(self, inputfilepath):

        try:
            sm = self.manager

            print("------------ solve SETTINGS :")
            print(SETTINGS)

            if SETTINGS["expert"]==1 :
                keep_value = int(self.ids.keep.value)
                border_value = int(self.ids.border.value)
                time_value = int(self.ids.time.value)
            else :
                print("NOT EXPERT : default values for keep border time")
                keep_value = INI['keep_default']
                border_value = INI['border_default']
                time_value = INI['time_default']

            print("*** solve savingoutputfile :")
            print("into SETTINGS = ", SETTINGS["savingoutputfile"])
            if SETTINGS["savingoutputfile"]==1 :
                outputfilepath = self.getname_outputfilepath(inputfilepath)
            else :
                outputfilepath = self.getname_outputfilepath()

            print("[INFO  ][Python] read_and_solve")

            if MODE=="LOCAL" :
                print("[INFO  ][Python] LOCAL MODE")
                args = {"model" : INI['model'],
                        "image" : inputfilepath,
                        "output" : outputfilepath,
                        "debug" : INI['debug'],
                        "keep" : keep_value, "border" : border_value,
                        "time" : time_value}
                read_and_solve(args)

            elif MODE=="WS" :
                print("[INFO  ][Python] WS MODE")
                read_and_solve(image=inputfilepath, output=outputfilepath,
                               keep=keep_value, border=border_value,
                               time=time_value)
            else : 
                error_msg(text="No MODE defined (WS, LOCAL) into code")

            print("[INFO  ][Python] outputfilepath : ", outputfilepath)

            if not is_a_file(outputfilepath):
                error_msg(text="Solution image file not built")

            screen = sm.screens[sm.number['displaysolution']]
            screen.ids.solutionpath.text = outputfilepath
            screen.ids.imagepath.text = inputfilepath

        except Exception as e :
            failed_msg(e)

class DisplaySolutionScreen(Screen):
    """Display the solution image file """
    pass

class CaptureImageScreen(Screen):
    """Capture and save the image file to be solved

    Buttons : play (camera on/off), capture

    Note : not finding a camera, perhaps because gstreamer is not installed,
    will throw an exception during the kv language processing.
    """

    def getname_inputfilepath(self, default=False):
        dirpath = SETTINGS["imagepath"]
        if default :
            in_name = "GRD.jpg" # default"
        else :
            timestr = time.strftime("%Y%m%d_%H%M%S")
            in_name = "GRD_{}.png".format(timestr)
        inputfilepath = os.path.join(dirpath, in_name)
        return inputfilepath

    def capture(self):
        try:
            sm = self.manager

            camera = self.ids['camera']

            if SETTINGS["savinginputfile"]==1 :
                inputfilepath = self.getname_inputfilepath(default=False)
            else :
                inputfilepath = self.getname_inputfilepath(default=True)

            camera.export_to_png(inputfilepath)
            print("[INFO][Python] Image captured, saved as image file ",
                  inputfilepath)

            screen = sm.screens[sm.number['displayimage']]
            screen.ids.imagepath.text = inputfilepath

        except Exception as e :
            failed_msg(e)

#------------------------------------------------------------------------------

class SolveSudokuScreenManager(ScreenManager):

    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        self.number = dict()

class SolveSudokuApp(App):

    #--------------------------------------------------------------------------
    # config and settings
    #--------------------------------------------------------------------------

    use_kivy_settings = True # False

    @classmethod
    def get_default_settings(cls) :
        """ Return default setting dictionary """

        return { 'expert': 0,
                 'imagepath': IMG_PATH,
                 'savingoutputfile': 1,
                 'savinginputfile': 0 }

    @classmethod
    def set_default_settings(cls, settings) :
        """ setting default values --> into settings"""

        S = cls.get_default_settings()
        for k in S.keys() :
            settings[k] = S[k]

    @classmethod
    def set_settings(cls, settings, config) :
        """ setting values from config --> into settings"""

        settings['expert'] = config.getint('app', 'expert')
        settings['imagepath'] = config.get('app', 'imagepath')
        settings['savingoutputfile'] = config.getint('app', 'savingoutputfile')
        settings['savinginputfile'] = config.getint('app', 'savinginputfile')

    def build_config(self, config): # before build()
        """ setting values into config

        Values from solvesudoku.ini if existing file, and else
        default values from here (and create file)

        config contains only setting values than can be modified
        """

        S = self.get_default_settings()
        config.setdefaults('app', S)

    def build_settings(self, settings): # called by open_settings()
        """Build Settings screen (+ Kivy by default) """

        settings.add_json_panel('App', self.config, data=json_data_app)

    def on_config_change(self, config, section, key, value):
        """ Update SETTINGS from config """

        super(SolveSudokuApp, self).on_config_change(config,
                                                     section, key, value)
        if section == 'app' :
            self.set_settings(SETTINGS, config)
            print("------------ end on_config_change SETTINGS :")
            print(SETTINGS)

    def reset_settings_msg(self, *args):
        layout = GridLayout(cols=1, padding=10)
        popup_label1  = Label(text="App is going to be closed\nfor RESET to be taken into account")
        popup_label2  = Label(text="Do you really want to RESET ?")
        validate_button = Button(text = "Confirm")
        close_button = Button(text = "Cancel")
        layout.add_widget(popup_label1)
        layout.add_widget(popup_label2)
        layout.add_widget(validate_button)
        layout.add_widget(close_button)       
        popup = Popup(title='RESET', content=layout, size_hint=(0.5, 0.5))
        popup.open()   
        validate_button.bind(on_press=self.reset_settings)
        close_button.bind(on_press=popup.dismiss)   

    def reset_settings(self, *args):
        inifile_path = get_config_file_name()
        try :
            if is_a_file(inifile_path):
                print("Remove file ", inifile_path, "...")
                os.remove(inifile_path)
                print(inifile_path, "removed")
            self.do_quit()
        except Exception as e :
            failed_msg(e)

    def close_settings(self, settings=None):
        super(SolveSudokuApp, self).close_settings(settings)
        self.root.current = 'main'

    #--------------------------------------------------------------------------

    def build(self):

        try:

            self.title = 'Solve Sudoku'

            self.settings_cls = SettingsWithTabbedPanel

            sm = SolveSudokuScreenManager()

            main_screen = MainScreen(name='main')
            sm.add_widget(main_screen)

            self.set_default_settings(SETTINGS)
            self.set_settings(SETTINGS, self.config)
            print("------------ build SETTINGS :")
            print(SETTINGS)

            set_screen = SetScreen(name='set')
            ids = set_screen.ids
            s = self.create_settings()
            ids.settings_content.add_widget(s)
            ids.reset_settings_msg.bind(on_press=self.reset_settings_msg)
            sm.add_widget(set_screen)

            captureimage_screen = CaptureImageScreen(name='captureimage')
            sm.add_widget(captureimage_screen)

            selectimagefile_screen = SelectImageFileScreen(
                                                        name='selectimagefile')
            ids = selectimagefile_screen.ids
            ids.fc.bind(selection=selectimagefile_screen.select_file)
            ids.fc.path = SETTINGS["imagepath"]
            sm.add_widget(selectimagefile_screen)

            displayimage_screen = DisplayImageScreen(name='displayimage')
            sm.add_widget(displayimage_screen)

            displaysolution_screen = DisplaySolutionScreen(
                                                        name='displaysolution')
            ids = displaysolution_screen.ids
            sm.add_widget(displaysolution_screen)

            for n,screen in enumerate(sm.screens) :
                sm.number[screen.name] = n

            return sm

        except Exception as e :
            failed_msg(e)


    def do_quit(self):
        SolveSudokuApp.get_running_app().stop()
        os._exit(0)

if __name__ == '__main__':
    Config.set('graphics', 'fullscreen', '0')
    Config.set('graphics', 'width', '350')
    Config.set('graphics', 'height', '50')
    Config.write()
    SolveSudokuApp().run()

