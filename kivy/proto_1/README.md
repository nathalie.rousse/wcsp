# solvesudoku.py with kivy :

solvesudoku.py : solve sudoku

  - by calling ws (in "WS" MODE case)
  - by calling local toulbar2_visual_sudoku_puzzle.py (in "LOCAL" MODE case)

## WS MODE case

- into solvesudoku.py code :

      MODE = "WS"

- install _kivy_venv_ws 

      pip3 install --upgrade pip wheel setuptools virtualenv
      python3 -m venv _kivy_venv_ws
      source _kivy_venv_ws/bin/activate
      pip3 install -r requirements_ws.txt

- run App :

      source _kivy_venv_mix/bin/activate
      python3 solvesudoku.py

## LOCAL MODE case

- into solvesudoku.py code :

      MODE = "LOCAL"

- install _kivy_venv_cpu

      pip3 install --upgrade pip wheel setuptools virtualenv
      python3 -m venv _kivy_venv_cpu
      source _kivy_venv_cpu/bin/activate
      pip3 install -r requirements_cpu.txt

- run App :

      source _kivy_venv_cpu/bin/activate
      python3 solvesudoku.py

## MEMO

-  TODO in ws/ws.py : send parameters keep, border, time into ws request

## Note

- install _kivy_venv_mix (for both WS and LOCAL MODEs)

      pip3 install --upgrade pip wheel setuptools virtualenv
      python3 -m venv _kivy_venv_mix
      source _kivy_venv_mix/bin/activate
      pip3 install -r requirements_ws.txt
      pip3 install -r requirements_cpu.txt

