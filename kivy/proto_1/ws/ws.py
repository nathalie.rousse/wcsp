""" read_and_solve calling ws request """

import pycurl
#import io
import json

def read_and_solve(image, output, keep=None, border=None, time=None) :
    """Sends POST request and return solution image file

    NOK : passage des parameters (data) : keep, border, time

Memo :
todownload="no"
returned_type ="stdout" ="stdout.txt" ="run.zip"

    """
    with open(output, 'wb') as solution_file:

        url_vsudoku = 'http://147.100.179.250/api/tool/vsudoku'
        c = pycurl.Curl()

        c.setopt(c.POST, 1)
        #c.setopt(c.HTTPHEADER, ['Accept: application/json',
        #                        'Content-Type:multipart/form-data'])
        c.setopt(c.HTTPHEADER, ['Content-Type:multipart/form-data'])
        c.setopt(c.URL, url_vsudoku)
        c.setopt(c.WRITEDATA, solution_file)

        # NOK
        d = {'returned_type':'stdout'} #d = {'returned_type':'run.zip'}
        if keep is not None :
            d['keep'] = keep
        if border is not None :
            d['border'] = border
        if time is not None :
            d['time'] = time
        #d = {'keep':77, 'border':44, 'time':9, 'returned_type':'run.zip'}
                                               #'returned_type':'stdout'}
        data = json.dumps(d)
        #send = [('file', (c.FORM_FILE, grid_file)), ('data', data), ]
        send = [('file', (c.FORM_FILE, image))]
        c.setopt(c.HTTPPOST, send)
        #c.setopt(c.VERBOSE, 1)
        c.perform()
        c.close()

#python3 toulbar2_visual_sudoku_puzzle.py -m digit_classifier.h5 -i sudoku_poster.jpg -o WSsolution_poster.jpg -k 70
#echo "[INFO] toulbar2_visual_sudoku_puzzle.py running with options : -i $1 -o solution.jpg -k $2 -b $3 -t $4"
#-i $1 -k $2 -b $3 -t $4

