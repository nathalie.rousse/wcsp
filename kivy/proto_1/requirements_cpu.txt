kivy[base]
#kivy_examples
#tensorflow
tensorflow-cpu
#tflite-runtime tensorflow lite
keras==2.9
numpy
sklearn
imutils
opencv-python
opencv-python-headless
scikit-image
pytoulbar2
