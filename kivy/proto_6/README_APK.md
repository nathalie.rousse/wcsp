# APK

## SEE :

https://kivy.org/doc/stable/guide/packaging-android.html#packaging-android

https://kivy.org/doc/stable/guide/packaging-android.html#release-on-the-market

you must run Buildozer with : buildozer android release
This creates a release AAB in the bin directory, which you must properly sign and zipalign. The procedure for doing this is described in the Android documentation at 
https://developer.android.com/studio/publish/app-signing.html#signing-manually
All the necessary tools come with the Android SDK.

https://developer.android.com/studio/publish/app-signing.html#signing-manually

- Install Android Studio
  (https://developer.android.com/studio)

Generate key
https://www.youtube.com/watch?v=vZ0Ar9JCua8

Generate key
https://www.youtube.com/watch?v=vZ0Ar9JCua8

Publier une application sur Google Play :
https://wiki.labomedia.org/index.php/Publier_une_application_sur_Google_Play.html

## Créer un compte développeur Google Play

Rendez-vous sur Google Play Developer Console.
https://accounts.google.com/signin/v2/identifier?service=androiddeveloper&passive=1209600&continue=https%3A%2F%2Fplay.google.com%2Fconsole%2Fsignup&followup=https%3A%2F%2Fplay.google.com%2Fconsole%2Fsignup&flowName=GlifWebSignIn&flowEntry=ServiceLogin

Connectez-vous avec votre adresse Gmail.

Acceptez les CGV.

Réglez les frais d'inscription : un paiement unique de 25 euros pour publier toutes les applications de votre choix !

Renseignez les informations relatives à votre compte : nom du développeur, adresse e-mail et numéro de téléphone. 

