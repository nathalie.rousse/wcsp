
# Simon de Givry @ INRAE, 2022
# adapted from a tutorial by Adrian Rosebrock (@ PyImageSearch, 2022)

# import the necessary packages
from visualsudoku.puzzle import extract_digit
from visualsudoku.puzzle import find_puzzle
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import math
import numpy as np
import argparse
import imutils
import cv2
import pytoulbar2

def read_and_solve(args) :
    print("[INFO] read_and_solve args = ", args)

    # load the digit classifier from disk
    print("[INFO] loading digit classifier...")
    model = load_model(args["model"])
    # load the input image from disk and resize it
    print("[INFO] processing image...")
    image = cv2.imread(args["image"])
#    image = imutils.resize(image, width=600)
    width=900
    (h, w) = image.shape[:2]
    r = width / float(w)
    dim = (width, int(h * r))
    image = cv2.resize(image, dim, interpolation = cv2.INTER_CUBIC)

    # find the puzzle in the image and then
    (puzzleImage, warped) = find_puzzle(image, debug=args["debug"] > 0)
    # initialize our 9x9 Sudoku board
    board = np.zeros((9, 9), dtype="int")
    # a Sudoku puzzle is a 9x9 grid (81 individual cells), so we can
    # infer the location of each cell by dividing the warped image
    # into a 9x9 grid
    stepX = warped.shape[1] // 9
    stepY = warped.shape[0] // 9
    # initialize a list to store the (x, y)-coordinates of each cell
    # location
    cellLocs = []
    prediction = []
    # loop over the grid locations
    for y in range(0, 9):
	    # initialize the current list of cell locations
	    row = []
	    for x in range(0, 9):
		    # compute the starting and ending (x, y)-coordinates of the
		    # current cell
		    startX = max(0, x * stepX - args["border"] * stepX // 100)
		    startY = max(0, y * stepY - args["border"] * stepY // 100)
		    endX = min(warped.shape[1], (x + 1) * stepX + args["border"] * stepX // 100)
		    endY = min(warped.shape[0], (y + 1) * stepY + args["border"] * stepY // 100)
		    # add the (x, y)-coordinates to our cell locations list
		    row.append((startX, startY, endX, endY))
    
    
		    # crop the cell from the warped transform image and then
		    # extract the digit from the cell
		    cell = warped[startY:endY, startX:endX]
		    digit = extract_digit(cell, debug=args["debug"] > 0, keep=args["keep"])
		    # verify that the digit is not empty
		    if digit is not None:
			    # resize the cell to 28x28 pixels and then prepare the
			    # cell for classification
			    roi = cv2.resize(digit, (28, 28))
			    roi = roi.astype("float") / 255.0
			    roi = img_to_array(roi)
			    roi = np.expand_dims(roi, axis=0)
			    # classify the digit and update the Sudoku board with the
			    # prediction
			    prediction.append(model.predict(roi))
			    board[y, x] = prediction[-1].argmax(axis=1)[0]
			    if args["debug"] > 0:
				    print(y, x, board[y, x], prediction[-1])
		    else:
			    prediction.append([])

	    # add the row to our cell locations
	    cellLocs.append(row)

    # construct a Sudoku puzzle from the board
    print("[INFO] OCR'd Sudoku board:")
    print(board)

    TOP=1000000000
    PRECISION=1000
    puzzle = pytoulbar2.CFN(TOP, vac=1)
    var = [[0 for v in range(9)] for v in range(9)]
    for row in range(9):
        for column in range(9):
            var[row][column] = puzzle.AddVariable('x' + str(row) + '_' + str(column), range(1, 10))

    for row in range(9):
        for col1 in range(9):
            for col2 in range(col1+1, 9):
                puzzle.AddFunction([var[row][col1], var[row][col2]], [(TOP if val1==val2 else 0) for val1 in range(1, 10) for val2 in range(1, 10)])

    for col in range(9):
        for row1 in range(9):
            for row2 in range(row1+1, 9):
                puzzle.AddFunction([var[row1][col], var[row2][col]], [(TOP if val1==val2 else 0) for val1 in range(1, 10) for val2 in range(1, 10)])

    for i in range(3):
        for j in range(3):
            for row1 in range(i*3, i*3+3):
                for col1 in range(j*3,j*3+3):
                    for row2 in range(i*3, i*3+3):
                        for col2 in range(j*3,j*3+3):
                            if row1*3+col1 < row2*3+col2:
                                puzzle.AddFunction([var[row1][col1], var[row2][col2]], [(TOP if val1==val2 else 0) for val1 in range(1, 10) for val2 in range(1, 10)])

    for row in range(9):
        for column in range(9):
            if board[row, column] > 0:
                puzzle.AddFunction([var[row][column]], [-int(PRECISION*math.log(prediction[row*9+column][0][i+1])) for i in range(9)])
                if args["debug"] > 0:
                    print(row, column, [-int(PRECISION*math.log(prediction[row*9+column][0][i+1])) for i in range(9)])

    # solve the Sudoku puzzle
    print("[INFO] solving Sudoku puzzle...")
    if args["debug"] > 0:
	    puzzle.Option.verbose = 0
    puzzle.CFN.timer(args["time"])
    solution = puzzle.Solve()[0]
    #print(solution)
    pos=0
    for row in range(9):
        for column in range(9):
            board[row, column] = solution[pos]
            pos += 1
    print(board)

    # loop over the cell locations and board
    for (cellRow, boardRow) in zip(cellLocs, board):
	    # loop over individual cell in the row
	    for (box, digit) in zip(cellRow, boardRow):
		    # unpack the cell coordinates
		    startX, startY, endX, endY = box
		    # compute the coordinates of where the digit will be drawn
		    # on the output puzzle image
		    textX = int((endX - startX) * 0.33)
		    textY = int((endY - startY) * -0.2)
		    textX += startX
		    textY += endY
		    # draw the result digit on the Sudoku puzzle image
		    cv2.putText(puzzleImage, str(digit), (textX, textY),
			    cv2.FONT_HERSHEY_SIMPLEX, 0.98, (0, 200, 255), 3)

    cv2.imwrite(args["output"], puzzleImage)

    # show the output image
    if args["debug"] > 0:
        cv2.imshow("Sudoku Result", puzzleImage)
        cv2.waitKey(0)

if __name__ == "__main__":

    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-m", "--model", required=True,
        help="path to trained digit classifier")
    ap.add_argument("-i", "--image", required=True,
        help="path to input Sudoku puzzle image")
    ap.add_argument("-o", "--output", required=True,
        help="path to ouput Sudoku solution image")
    ap.add_argument("-d", "--debug", type=int, default=-1,
        help="whether or not we are visualizing each step of the pipeline")
    ap.add_argument("-k", "--keep", type=int, default=0,
        help="keep percentage of the center of cell images without removing grid lines")
    ap.add_argument("-b", "--border", type=int, default=0,
        help="enlarge the cell image region by some extra percentage")
    ap.add_argument("-t", "--time", type=int, default=5,
        help="CPU time limit in seconds for solving sudoku")
    args = vars(ap.parse_args())

    read_and_solve(args)

