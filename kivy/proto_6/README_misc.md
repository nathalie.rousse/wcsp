# MISC (info)

## Kivy 

- https://kivy.org/

- Kivy est une bibliothèque libre et open source pour Python, utile pour créer des applications tactiles pourvues d'une interface utilisateur naturelle. Cette bibliothèque fonctionne sur Android, iOS, GNU/Linux, OS X et Windows. Elle est distribuée gratuitement et sous licence MIT.

- https://kivy.org/doc/stable/guide/packaging-android.html

- http://tableauxmaths.fr/spip/spip.php?article135#Kivy-sur-Android

## Kivy - Android

- Kivy sur Android, 2 methodes :
  http://tableauxmaths.fr/spip/spip.php?article135#Kivy-sur-Android

  A. Installer Kivy Launcher sur votre tablette ou smartphone :
    Vous pourrez alors transférer vos fichiers .py sur votre appareil et les
    lancer. Méthode servant à tester rapidement des programmes simples sur
    Android.

  B. Créer une application pour Android (apk) :
     Installer buildozer puis l'utiliser pour générer l'app

- Tuto en français :
  https://www.youtube.com/watch?v=ykCUSLV0pdo
  18:21: Installation sur un smartphone

- https://kivy.org/doc/stable/gettingstarted/packaging.html
  https://kivy.org/doc/stable/guide/packaging-android.html

- Commandes buildozer :

  - buildozer target command

    buildozer android clean
    buildozer android update
    buildozer android deploy
    buildozer android debug
    buildozer android release

  - or all in one (compile in debug, deploy on device)

    buildozer android debug deploy

  - set the default command if nothing set

    buildozer setdefault android debug deploy run

- Convert Kivy To Android App Using Buildozer 2021(Lightning Tutorial)

  https://www.youtube.com/watch?v=Ir_35vdchNo

APK => SEE : README_APK.md

## CI/CD ?

https://github.com/kivy/buildozer --> Buildozer GitHub action

Use ArtemSBulgakov/buildozer-action@v1 (https://github.com/ArtemSBulgakov/buildozer-action) to build your packages automatically on push or pull request. See full workflow example (https://github.com/ArtemSBulgakov/buildozer-action#full-workflow).

