# LOCAL MODE case

# TensorFlow (info)

- https://www.tensorflow.org/lite

- https://www.tensorflow.org/lite/guide/python

- Installer TensorFlow Lite pour Python : python3 -m pip install tflite-runtime

- requirements.txt : tensorflow vs tensorflow-cpu

# TensorFlow lite (info)

From TensorFlow --> to TensorFlow lite :

- Convertisseur TensorFlow Lite

  https://www.tensorflow.org/lite/convert/index#python_api

  Le convertisseur TensorFlow Lite utilise un modèle TensorFlow et génère un
  modèle TensorFlow Lite

- Inférence TensorFlow Lite (interpreteur)

  https://www.tensorflow.org/lite/guide/inference

  processus d'exécution d'un modèle tensorflow Lite sur l'appareil afin de
  faire des prédictions basées sur des données d'entrée

# python-for-android

## Python libraries calling C++ (pytoulbar2, pycurl...)

https://python-for-android.readthedocs.io/en/latest/contribute/#create-your-own-recipes

https://stackoverflow.com/questions/17746617/kivy-python-for-android-c

https://groups.google.com/g/kivy-users/c/X-tk3rTQ638
-->
https://github.com/kivy/python-for-android/tree/master/pythonforandroid/recipes
-->
https://github.com/kivy/python-for-android/blob/master/pythonforandroid/recipes/libcurl/__init__.py
https://github.com/kivy/python-for-android/tree/master/pythonforandroid/recipes/numpy
https://github.com/kivy/python-for-android/blob/master/pythonforandroid/recipes/dateutil/__init__.py
...

Camera4Kivy Tensoflow Lite Example :
https://github.com/Android-for-Python/c4k_tflite_example

# Essai pytoulbar2 (C++ library) Under Android

  Dans buildozer.spec :
  requirements = python3,kivy,pytoulbar2

  buildozer -v android debug deploy run logcat | grep python
  =>
  /app/.buildozer/android/platform/build-arm64-v8a_armeabi-v7a/build/python-installs/solvesudoku/arm64-v8a/pytoulbar2 :
   __init__.py __init__.pyc
   pytb2.cpython-38-x86_64-linux-gnu.so*
   pytoulbar2.py pytoulbar2.pyc

  app/.buildozer/android/platform/build-arm64-v8a_armeabi-v7a/build/python-installs/solvesudoku/armeabi-v7a/pytoulbar2 :
   __init__.py __init__.pyc
   pytb2.cpython-38-x86_64-linux-gnu.so*
   pytoulbar2.py pytoulbar2.pyc

  Dans main.py :
  import pytoulbar2

  buildozer -v android debug deploy run logcat | grep python

  06-03 15:40:56.650 19656 19920 I python  :  ImportError: dlopen failed: "/data/user/0/org.test.solvesudoku/files/app/_python_bundle/site-packages/pytoulbar2/pytb2.so" is for EM_X86_64 (62) instead of EM_AARCH64 (183)

