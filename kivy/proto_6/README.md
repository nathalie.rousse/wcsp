# SolveSudokuApp with kivy

# Desc

main.py App developed with kivy, calling toulbar2_visual_sudoku_puzzle.py.

toulbar2_visual_sudoku_puzzle.py :

  - from ws : first step, current version.
  - local : later, to be tested/updated.

# App :

  - code : 'app' folder

  - main.py solves sudoku :

    - by calling ws web services, in WS MODE case
      (MODE = "WS" into main.py code)

    - by calling local toulbar2_visual_sudoku_puzzle.py, in LOCAL MODE case
      (MODE = "LOCAL" into main.py code)

  - solvesudoku.kv

# Python virtual environment :

  - create _kivy_venv

      pip3 install --upgrade pip wheel setuptools virtualenv
      python3 -m venv _kivy_venv
      source _kivy_venv/bin/activate

  - for App and kivy in WS MODE case : pip3 install -r requirements_ws.txt

  - for buildozer : pip3 install -r requirements_buildozer.txt

  - Note : in LOCAL MODE case :
         - !!! mv visualsudoku app/.
         - for App and kivy : pip3 install -r requirements_local.txt

# Linux - Run App in WS MODE case :

  - requirements_ws.txt required

  - commands :

      source _kivy_venv/bin/activate
      python3 main.py

# Android - Run App in WS MODE case (Debug) :

  - requirements_ws.txt and requirements_buildozer.txt  required

  - prepare smartphone :

    - Activate "options de développement" (cf "numéro de version")
    - And into "paramètres développeur" : USB debugging, Stay awake

  - build install and run apk :

    - create :

      source _kivy_venv/bin/activate
      cd app
      buildozer init

          => .buildozer , buildozer.spec  ... modify ...

    - plug in your android device and run :
      !!! Smartphone connected with PC/Linux (debug screen)

      buildozer android debug deploy run

          => bin/solvesudoku-0.1-arm64-v8a_armeabi-v7a-debug.apk

      buildozer -v android debug
      buildozer android deploy run logcat
      buildozer -v android debug deploy run logcat > my_log.txt
      buildozer -v android debug deploy run logcat | grep App

# Android - APK delivery (WS MODE case) :

TODO

# Memo - toulbar2_visual_sudoku_puzzle.py

  - Required :

      #pip3 install --quiet --upgrade pip wheel setuptools
      #pip3 install --quiet --upgrade tensorflow-cpu
      #pip3 install --quiet --upgrade keras
      #pip3 install --quiet --upgrade numpy
      #pip3 install --quiet --upgrade sklearn
      #pip3 install --quiet --upgrade imutils
      #pip3 install --quiet --upgrade opencv-python
      #pip3 install --quiet --upgrade opencv-python-headless
      #pip3 install --quiet --upgrade scikit-image

  - Command :
      python3 toulbar2_visual_sudoku_puzzle.py -m digit_classifier.h5 -i sudoku.png -o solution.jpg -k 40 -b 15 -t 10

  - Run :

      source _kivy_venv/bin/activate
      cd run
      python3 visualsudoku/toulbar2_visual_sudoku_puzzle.py -m visualsudoku/digit_classifier.h5 -i img/sudoku_poster.jpg -o solution.jpg -k 40 -b 15 -t 10

