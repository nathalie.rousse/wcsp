
# PAGE choisir image existante (select + afficher) ... solve ... afficher image solution

# AJOUTER bouton 'solve'

import kivy  
from kivy.app import App 
kivy.require('1.9.0') 
from kivy.uix.boxlayout import BoxLayout
 
from visualsudoku.toulbar2_visual_sudoku_puzzle import read_and_solve

class Filechooser(BoxLayout):

    def select(self, *args):

        try:
            print("************************")
            print("SELECT args : ", args)
            inputfilepath = args[1][0]
            self.label.text = inputfilepath
            print("[INFO  ][Python] inputfilepath : ", inputfilepath)
            self.ids.imageView.source = inputfilepath
        except:
            pass
 
    def solve(self, *args):

        try:
            inputfilepath = self.label.text
            outputfilepath = "img/SOL_ZZZ.png"

            args = {"model" : "visualsudoku/digit_classifier.h5",
                    "image" : inputfilepath, # "img/sudoku_poster.jpg",
                    "output" : outputfilepath,
                    "debug" : 0,
                    "keep" : 40, "border" : 15, "time" : 10,}

            print("[INFO  ][Python] read_and_solve")
            read_and_solve(args)
            print("[INFO  ][Python] outputfilepath : ", outputfilepath)
            self.ids.imageView.source = outputfilepath

        except:
            pass
 

class FileApp(App):

    def build(self):
        return Filechooser()
 
if __name__ == '__main__':
    FileApp().run()

