# toulbar2_visual_sudoku_puzzle.py with kivy

## Commands

- install _kivy_venv :

      pip3 install --upgrade pip wheel setuptools virtualenv
      python3 -m venv _kivy_venv
      source _kivy_venv/bin/activate
      pip3 install -r requirements.txt

- run App :

      source _kivy_venv/bin/activate
      python3 main_camera.py
      python3 main_filechooser.py

## Used

- Kivy :

  https://kivy.org/

  Kivy est une bibliothèque libre et open source pour Python, utile pour créer des applications tactiles pourvues d'une interface utilisateur naturelle. Cette bibliothèque fonctionne sur Android, iOS, GNU/Linux, OS X et Windows. Elle est distribuée gratuitement et sous licence MIT.

  https://kivy.org/doc/stable/guide/packaging-android.html

  http://tableauxmaths.fr/spip/spip.php?article135#Kivy-sur-Android

- TensorFlow :
  
  https://www.tensorflow.org/lite

  https://www.tensorflow.org/lite/guide/python

  Installer TensorFlow Lite pour Python : python3 -m pip install tflite-runtime


# NOTES (TRAVAIL EN COURS)

## Memo toulbar2_visual_sudoku_puzzle.py

- Required :

      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade pip wheel setuptools
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade tensorflow
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade keras
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade numpy
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade sklearn
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade imutils
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade opencv-python
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade opencv-python-headless
      #pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade scikit-image

- Command :

      python3 toulbar2_visual_sudoku_puzzle.py -m digit_classifier.h5 -i sudoku.png -o solution.jpg -k 40 -b 15 -t 10

- Run :

      source _kivy_venv/bin/activate
      cd run
      #python3 ../visualsudoku/src/toulbar2_visual_sudoku_puzzle.py -m ../visualsudoku/src/digit_classifier.h5 -i ../img/sudoku_poster.jpg -o solution.jpg -k 40 -b 15 -t 10

      python3 visualsudoku/toulbar2_visual_sudoku_puzzle.py -m visualsudoku/digit_classifier.h5 -i img/sudoku_poster.jpg -o solution.jpg -k 40 -b 15 -t 10

