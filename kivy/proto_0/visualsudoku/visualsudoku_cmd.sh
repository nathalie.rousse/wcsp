#!/bin/bash

###############################################################################
# Source : Simon de Givry @ INRAE, 2022
# adapted from a tutorial by Adrian Rosebrock (@ PyImageSearch, 2022)
###############################################################################

PYTHONPATH=$PYTHONPATH:/WS:/WS/pythonplus
export PYTHONPATH

# visualsudoku specific required
#already into pytoulbar2plus
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade pip wheel setuptools
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade tensorflow
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade keras
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade numpy
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade sklearn
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade imutils
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade opencv-python
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade opencv-python-headless
#pip3 install --quiet --cache-dir /WS/cache -t /WS/pythonplus --upgrade scikit-image

# run
cd /WS
#python3 toulbar2_visual_sudoku_puzzle.py -m digit_classifier.h5 -i sudoku_poster.jpg -o WSsolution_poster.jpg -k 70
echo "[INFO] toulbar2_visual_sudoku_puzzle.py running with options : -i $1 -o solution.jpg -k $2 -b $3 -t $4"
python3 toulbar2_visual_sudoku_puzzle.py -m digit_classifier.h5 -i $1 -o solution.jpg -k $2 -b $3 -t $4

# clean
rm -fr /WS/pythonplus
rm -fr /WS/cache

