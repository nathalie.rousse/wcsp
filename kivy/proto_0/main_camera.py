
# PAGE prendre photo grille ... solve ... afficher image solution

# ajouter bouton 'solve'
# Ajouter image solution (rangee sous img)

'''
Camera
======

Bouton 'play' : camera on/off

Bouton 'capture' : saves partial grid image file (under 'img'), calls toulbar2_visual_sudoku_puzzle.py function, (todo) displays solution image file (saved under 'img').
Note : not finding a camera, perhaps because gstreamer is not installed, will
throw an exception during the kv language processing.

'''

# Uncomment these lines to see all the messages
# from kivy.logger import Logger
# import logging
# Logger.setLevel(logging.TRACE)

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
import time

from visualsudoku.toulbar2_visual_sudoku_puzzle import read_and_solve

Builder.load_string('''
<CameraClick>:

    orientation: 'vertical'

    Camera:
        id: camera
        resolution: (640, 480)
        play: False

    ToggleButton:
        text: 'Play'
        on_press: camera.play = not camera.play
        size_hint_y: None
        height: '48dp'

    Button:
        text: 'Capture'
        size_hint_y: None
        height: '48dp'
        on_press: root.capture()
''')


class CameraClick(BoxLayout):
    def capture(self):
        '''
        Capture, save, and solve the image
        '''
        try:

            camera = self.ids['camera']

            timestr = time.strftime("%Y%m%d_%H%M%S")
            inputfilepath = "img/GRD_{}.png".format(timestr)
            outputfilepath = "img/SOL_{}.png".format(timestr)
        
            camera.export_to_png(inputfilepath)
            print("[INFO][Python] ",
                  "Partial grid captured, saved as image file ", inputfilepath)

            args = {"model" : "visualsudoku/digit_classifier.h5",
                    "image" : inputfilepath, #"img/sudoku_poster.jpg",
                    "output" : outputfilepath,
                    "debug" : 0,
                    "keep" : 40, "border" : 15, "time" : 10,}

            read_and_solve(args)
            print("[INFO][Python]")
            print("Solution found, saved as image file ", outputfilepath)
        
        except:
            pass

class TestCamera(App):

    def build(self):
        return CameraClick()

if __name__ == '__main__':
    TestCamera().run()

