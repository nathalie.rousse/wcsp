# wcsp

About Weighted Constraint Satisfaction Problem (WCSP).
Réseaux de contraintes pondérées, Réseaux de fonctions de coût.

Work done around toulbar2 : solver for cost function networks.

Install, use, call toulbar2
===========================

- to install/use toulbar2 :

  - installation locale
  - installation sur serveur tapou
  - travail dans une machine virtuelle
  - travail dans un container (docker, singularity)
  - appel de services web

  See [ways_to_install_toulbar2](./ways_to_install_toulbar2)

- to call toulbar2 :

  - by C++ code
  - by python code
  - by http requests/responses

  See [ways_to_call_toulbar2](./ways_to_call_toulbar2)

Continuous Integration
======================

See [CI folder](./CI) : 

- [README.md](./CI/README.md) | [EXAMPLES.md](.CI/EXAMPLES.md/) |
  [docker](.CI/docker) and [docker_use](./CI/docker_use) folders

Some delivery to https://github.com/toulbar2/toulbar2 CI/CD

Android App
===========

Before developing [VisualSudokuApp](https://github.com/toulbar2/visualsudoku) 
(https://github.com/toulbar2/visualsudoku) :

- [kivy folder](./kivy) | [visualsudoku folder](./visualsudoku)

[Android](./android) environment

