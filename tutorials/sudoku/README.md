# sudoku_tut.py file 

Description
===========

The sudoku_tut.py file has been written from the existing sudoku.py file
(copied here) where some "print" have been added in order to describe the
sudoku problem, instance and resolution.

- To remove print commands of sudoku_tut.py, replace into code :

      tut = True

  by :

      tut = False

- To choose the grid to solve (among the 18000 ones that are defined), change
  into code the INDICE value by a value in 0.. 17999.

Command to run sudoku_tut.py
============================

 - Prerequisite :

       toulbar2 and pytoulbar2 installed

       ln -s jupyter_notebook/valid.csv.xz valid.csv.xz

 - with comments on screen :

       python3 sudoku_tut.py

 - with comments into file :

       python3 sudoku_tut.py > sudoku_tut.txt

