import pytoulbar2,CFN
import numpy as np
import itertools
import pandas as pd

tut = True # tut = False
def tut_print(*args):
    if tut : print(*args)

# Adds a clique of differences with violation "cost" on "varList"
def addCliqueAllDiff(theCFN, varList, cost):
    different = (cost*np.identity(size, dtype=np.int64)).flatten()
    for vp in itertools.combinations(varList,2):
        theCFN.AddFunction(vp,different)

def tut_sel(variable_indice):
    return (variable_indice==0 or variable_indice==7 or variable_indice==10)

# Sets the value of variable with index "vIdx" to "value" using a unary function
def setHint(theCFN,vIdx,value):
    costs = myCFN.GetUB()*np.ones(size, dtype = np.int64)
    costs[value-1] = 0
    if tut_sel(v) :
        tut_print("    ", costs, "valuing 0 for", value)
    theCFN.AddFunction([vIdx], costs)

def tut_printGrid(l):
    s = "   "
    for i,v in enumerate(l):
        #print(v,end=(' ' if (i+1)%size else '\n'))
        if (i+1)%size :
            s += str(v) + ' '
        else :
            s += str(v)
            tut_print(s)
            s = "   "

def printGrid(l):
    for i,v in enumerate(l):
        print(v,end=(' ' if (i+1)%size else '\n'))

tut_print("***************************************************************")
tut_print("*                                                             *")
tut_print("* Problem definition                                          *")
tut_print("*                                                             *")
tut_print("***************************************************************")
tut_print("")
tut_print("The n×n Sudoku problem is defined over a grid of n^2 × n^2 cells that each contains a number between 1 and n^2. This grid is subdivided in n^2 sub-grids of size n × n. A solved Sudoku grid is such that the numbers in every row, column and n × n sub-grids are all different.")

myCFN = CFN.CFN(1)

# Sudoku size parameter (typical 3 gives 3*3 x 3*3 grid)
par = 3
size = par * par

tut_print("")
tut_print("In the particular case of n=", par, ", ", "n^2=", size)
tut_print("")
tut_print("The", par, "×", par, "Sudoku problem is defined over a grid of", size, "×", size, "=", size*size, "cells that each contains a number between 1 and", size, ". This grid is subdivided in ", size, "sub-grids of size", par, "×", par, ". A solved Sudoku grid is such that the numbers in every row, column and", par, "×", par, "sub-grids are all different.")

tut_print("")
tut_print("***************************************************************")
tut_print("*                                                             *")
tut_print("* Builds the problem                                           ")
tut_print("*                                                             *")
tut_print("***************************************************************")
tut_print("")

# list of row, column and cells variable indices
rows = [ [] for _ in range(size) ]
columns = [ [] for _ in range(size) ]
cells = [ [] for _ in range(size) ]

# create variables and keep indices in row, columns and cells 
tut_indice = 0
tut_names = list()
tut_indices = list()
for i in range(size):
    for j in range(size):
        vIdx = myCFN.AddVariable("X"+str(i+1)+"."+str(j+1),range(1,size+1))

        ind_txt = ""
        if tut_indice < 10 : ind_txt = " "
        tut_names.append("X"+str(i+1)+"."+str(j+1))
        tut_indices.append(ind_txt+str(tut_indice))
        tut_indice = tut_indice + 1

        columns[j].append(vIdx)
        rows[i].append(vIdx)
        cells[(i//par)*par+(j//par)].append(vIdx)

tut_print("-> Create", size*size, "variables :")
tut_printGrid(tut_names)
tut_print("")
tut_print("   ... with such indices :")
tut_printGrid(tut_indices)
tut_print("")
tut_print("-> Keep those", size*size, "variables indices in rows, columns and cells (each listing", size, "lists of", size, "variables indices). Those 3*", size, "=", 3*size, "lists of", size, "indices correspond with the sets of variables whose values have to be different (every row, every column and every sub-grids).")

tut_print("")
tut_print(" rows = [")
for row in rows : tut_print("  ", row)
tut_print(" ]")
tut_print(" columns = [")
for column in columns : tut_print("  ", column)
tut_print(" ]")
tut_print(" cells = [")
for cell in cells : tut_print("  ", cell)
tut_print(" ]")
tut_print("")

tut_print("-> Add the clique constraints on rows, columns and cells :")
tut_print("   addCliqueAllDiff for each row, each column, each cell.")
# add the clique constraints on rows, columns and cells
for scope in rows+columns+cells:
    addCliqueAllDiff(myCFN,scope, myCFN.GetUB())    

tut_print("")
tut_print("***************************************************************")
tut_print("*                                                             *")
tut_print("* Solving some grids                                          *")
tut_print("*                                                             *")
tut_print("***************************************************************")

# Prefilled grids/solutions from the validation set of the RRN paper (0 meaning unknown)
valid = pd.read_csv("valid.csv.xz",sep=",", header=None).values
hints = valid[:][:,0]
sols = valid[:][:,1]

tut_print("")
tut_print("Here are some prefilled grids/solutions coming from the validation set of the RRN paper (0 meaning unknown) :")
tut_print("")
tut_print(" List of", len(hints), "hints :")
tut_print(" hints = ", hints)
tut_print("")
tut_print(" List of their solutions :")
tut_print(" sols = ", sols)
tut_print("")

INDICE = 34
grid = [int(h) for h in hints[INDICE]]
given_solution = [int(s) for s in sols[INDICE]]

tut_print("Let's solve one of those cases, hints[i] where i in 0..", (len(hints)-1), ".")
tut_print("")
tut_print("***************************************************************")
tut_print("")
tut_print("For example let's solve hints[",INDICE,"] :")
tut_print("")
tut_print(" hints[",INDICE,"] =", "\n", hints[INDICE])
tut_print("")
tut_print("grid = ", grid)
tut_print("")
tut_print("-> Creation of costs functions to represent the values predefinition : ")
tut_print("")
tut_print("   Predefined values into grid (0 for undefined) are :")
tut_printGrid(grid)
tut_print("")
tut_print("   (memo) Variables names are :")
tut_printGrid(tut_names)
tut_print("")
tut_print("   (memo) Variables indices are :")
tut_printGrid(tut_indices)
tut_print("")

# fill-in hints: a string of values, 0 denote empty cells
tut_print("   For each variable whose value is known (predefined into grid),")
tut_print("   creation of a cost function equal to 0 for its value and equal")
tut_print("   to UB else.")
tut_indices_costs = list()
tut_names_costs = list()
tut_print("")
tut_print("   For example :")
for v,h in enumerate(grid):
    if h:
        tut_indices_costs.append(v)
        tut_names_costs.append(tut_names[v])
        if tut_sel(v) :
            tut_print("")
            tut_print("   - Variable", tut_names[v], "of", v, "indice, with known value=", h, ":")
            cf_name = "F_" + tut_names[v]
            tut_print("     Cost function", cf_name, " created (with", cf_name, "[", h,"] = 0) :")
        setHint(myCFN,v,h)
tut_print("")
tut_print("   => A cost function created for variables and their indices :")
tut_print("     ", tut_names_costs)
tut_print("     ", tut_indices_costs)
tut_print("")

tut_print("-> Solve...")
sol = myCFN.Solve()

if not tut :
    printGrid(sol[0])

tut_print("")
tut_print("=> Solution found :")
tut_printGrid(sol[0])
tut_print("")
tut_print("(memo) Predefined values into grid :")
tut_printGrid(grid)
tut_print("")
tut_print("(memo) The solution given with grid was :")
tut_printGrid(given_solution)

tut_print("")
tut_print("***************************************************************")
tut_print("")

