
import time

def timer_init() :
    return (time.time(), None) # (start, end)
    
def timer_stop(timer) :
    (start, end) = timer
    return (start, time.time()) # (start, end)
    
def timer_duration(timer) :
    (start, end) = timer
    return (end - start) # duration

