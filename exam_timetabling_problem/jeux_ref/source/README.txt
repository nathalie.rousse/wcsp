###############################################################################
# 
#                         source jeux_ref
#
###############################################################################

###############################################################################
# Desc

Rassemblement dans ce repertoire exam_timetabling_problem/jeux_ref/source :
- des donnees creees (nouveaux jeux)
- de l'existant (copie) qui sera utilise (code des modeles compare...)

###############################################################################
# Mode operatoire (create, updates...)

source SETTINGS.sh

JEUX_REF_SOURCE_PATH=${ROOT_PATH}/jeux_ref/source

#------------------------------------------------------------------------------
Code toulbar2 (exam_tt_pb.py, exam_tt_main.py...) : 

has been copied from wcsp/exam_timetabling_problem/toulbar2
then exam_tt_main.py may has been modified

  diff ${ROOT_PATH}/toulbar2/exam_tt_main.py ${JEUX_REF_SOURCE_PATH}/toulbar2/.
  diff ${ROOT_PATH}/toulbar2/exam_tt_pb.py ${JEUX_REF_SOURCE_PATH}/toulbar2/.
  diff ${ROOT_PATH}/toulbar2/measure.py ${JEUX_REF_SOURCE_PATH}/toulbar2/.
  diff ${ROOT_PATH}/toulbar2/result.py ${JEUX_REF_SOURCE_PATH}/toulbar2/.
  diff ${ROOT_PATH}/toulbar2/compare.py ${JEUX_REF_SOURCE_PATH}/toulbar2/.
  diff ${ROOT_PATH}/toulbar2/ws/exam_tt_cmd.sh ${JEUX_REF_SOURCE_PATH}/toulbar2/.

#------------------------------------------------------------------------------
Code minizinc (exam_tt.mzn) :

has been copied from wcsp/exam_timetabling_problem/minizinc

  diff ${ROOT_PATH}/minizinc/exam_tt.mzn ${JEUX_REF_SOURCE_PATH}/minizinc/.
  diff ${ROOT_PATH}/minizinc/ws/exam_tt_cmd.sh ${JEUX_REF_SOURCE_PATH}/minizinc/.

#------------------------------------------------------------------------------
Data (minizinc, toulbar2) :

- toy has been copied from ${DATA_DZN_PATH} ${DATA_PY_PATH} ${DATA_JSONPY_PATH}

  diff ${DATA_DZN_PATH}/toy.dzn ${JEUX_REF_SOURCE_PATH}/data/.
  diff ${DATA_PY_PATH}/toy.py ${JEUX_REF_SOURCE_PATH}/data/.
  diff ${DATA_JSONPY_PATH}/toyJSON.py ${JEUX_REF_SOURCE_PATH}/data/.

- toybis has been created here from toy :
  toybis.py modified from toy.py
  toybisJSON.py = toyJSON.py
  toy.dzn TODO from toy.dzn and toybis.py

#------------------------------------------------------------------------------

