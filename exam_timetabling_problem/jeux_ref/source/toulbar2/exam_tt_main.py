
import sys
from pytoulbar2 import CFN

from exam_tt_pb import build_exam_tt_problem
from measure import timer_init, timer_stop, timer_duration
from result import brut_result, pr_result, extract_results

# Options

OPTION_TIME = False
OPTION_VERBOSE = True
OPTION_VERBOSE_PLUS = False

version_name = "PRPRPR_3" # default
if len(sys.argv) > 1 :
    version_name = sys.argv[1]

from data import * # Events, Periods, Rooms, RoomedEvent, Conflicts,
                   # DistanceWeight, MinDistance, MaxDistance, Precedence,
                   # EventPeriodConstraints, EventRoomConstraints,
                   # RoomPeriodConstraints, RoomsetOverlap

if OPTION_VERBOSE_PLUS :
    print("--------- DATA ---------")
    print("Events=", Events, " ; ",
          "Periods=", Periods, " ; ", "Rooms=", Rooms)
    print("RoomedEvent=", RoomedEvent)
    print("Conflicts=", Conflicts)
    print("DistanceWeight=", DistanceWeight)
    print("MinDistance=", MinDistance)
    print("MaxDistance=", MaxDistance)
    print("Precedence=", Precedence)
    print("EventPeriodConstraints=", EventPeriodConstraints)
    print("EventRoomConstraints=", EventRoomConstraints)
    print("RoomPeriodConstraints=", RoomPeriodConstraints)
    print("RoomsetOverlap=", RoomsetOverlap)
    print("------------------------")

if OPTION_TIME :
    print("\nWarning : bad durations measures !!!")

Problem = None

#------------------------------------------------------------------------------
#                        Build Problem
#------------------------------------------------------------------------------

if OPTION_VERBOSE :
    print("\nBuild Problem ...")

if OPTION_TIME :
    timer_build_problem = timer_init()

Problem = build_exam_tt_problem(version_name=version_name,
                                MAXCOST_VALUE=10000)

if OPTION_TIME :
    timer_build_problem = timer_stop(timer_build_problem)

#------------------------------------------------------------------------------
#                        Save Problem
#------------------------------------------------------------------------------
if True :

    if OPTION_VERBOSE :
        print("\nSave Problem ...")

    #Problem.Dump('data.wcsp')
    #Problem.Dump('data.cfn')
    Problem.Dump('data'+'__'+version_name+'.wcsp')
    Problem.Dump('data'+'__'+version_name+'.cfn')

#------------------------------------------------------------------------------
#                        Solve
#------------------------------------------------------------------------------
if True :

    if OPTION_VERBOSE :
        print("\nSolve Problem ...")

    if OPTION_TIME :
        timer_solve_problem = timer_init()

    res = None

    Problem.Option.verbose = 0

    # tb2 option -vacint : VAC-integrality/Full-EAC variable ordering heuristic
    Problem.Option.FullEAC = True

    #Problem.Option.showSolutions = 0
    Problem.Option.showSolutions = 1

    #Problem.NoPreprocessing()
    #if version_name != "EEE" :
    #    Problem.Option.nbDecisionVars = 2*Events
    #else :
    #    Problem.Option.nbDecisionVars = Events

    # tb2 option -timer=[integer] : CPU time limit in seconds
    #Problem.CFN.timer(3600)

    res = Problem.Solve()

    if OPTION_TIME :
        timer_solve_problem = timer_stop(timer_solve_problem)

    if OPTION_TIME :
        print("\n----------- Durations (seconds) :")
        print("build_problem duration :", timer_duration(timer_build_problem))
        print("solve_problem duration :", timer_duration(timer_solve_problem))

    s = brut_result(res)

    s_pr = pr_result(s, version_name, Events, Periods, Rooms)

    if OPTION_VERBOSE_PLUS : #if OPTION_VERBOSE :
        extract_results(s_pr, Rooms)

#------------------------------------------------------------------------------
#  
#  solve minimize ConflictCost + RoomPreferenceCost + PeriodPreferenceCost + RoomPeriodCost + MinDirectionalDistanceCost + MaxDirectionalDistanceCost + MinUndirectionalDistanceCost + MaxUndirectionalDistanceCost;
#  
#  %solve::int_search(EventPeriod ++ EventRoom, first_fail, indomain_random, complete) minimize ConflictCost + RoomPreferenceCost + PeriodPreferenceCost + MinDirectionalDistanceCost + MaxDirectionalDistanceCost + MinUndirectionalDistanceCost + MaxUndirectionalDistanceCost;
#  
# % output ["Periods = "] ++ [show(EventPeriod[e] - 1) ++ " " | e in 1..Events] 
# %     ++ ["\nRooms = "] ++ [show(EventRoom[e] - 1) ++ " " | e in 1..Events]
# %     ++ ["\nConflictCost = \(ConflictCost)\n" ++ 
# %         "RoomPreferenceCost = \(RoomPreferenceCost)\n" ++ 
# %         "PeriodPreferenceCost = \(PeriodPreferenceCost)\n" ++ 
# %         "MinDirectionalDistanceCost = \(MinDirectionalDistanceCost)\n" ++
# %         "MaxDirectionalDistanceCost = \(MaxDirectionalDistanceCost)\n" ++
# %         "MinUndirectionalDistanceCost = \(MinUndirectionalDistanceCost)\n" ++
# %         "MaxUndirectionalDistanceCost = \(MaxUndirectionalDistanceCost)\n"];
#  
# output ["Periods = \(EventPeriod);\n" ++ 
#          "Rooms = \(EventRoom);\n" ++
#          "- ConflictCost = \(ConflictCost);\n" ++ 
#          "- RoomPreferenceCost = \(RoomPreferenceCost);\n" ++ 
#          "- PeriodPreferenceCost = \(PeriodPreferenceCost);\n" ++ 
#          "- RoomPeriodCost = \(RoomPeriodCost);\n" ++
#   "- DistanceCost = \(MinDirectionalDistanceCost + MaxDirectionalDistanceCost 
#   + MinUndirectionalDistanceCost + MaxUndirectionalDistanceCost);\n" ++
#   + MinDirectionalDistanceCost = \(MinDirectionalDistanceCost);\n" ++
#   "  + MaxDirectionalDistanceCost = \(MaxDirectionalDistanceCost);\n" ++
#   "  + MinUndirectionalDistanceCost = \(MinUndirectionalDistanceCost);\n" ++
#   "  + MaxUndirectionalDistanceCost = \(MaxUndirectionalDistanceCost);\n"
#         ]
#  
#------------------------------------------------------------------------------

