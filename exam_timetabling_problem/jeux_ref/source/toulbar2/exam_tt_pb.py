
from pytoulbar2 import CFN

# from exam_tt.mzn + .dzn

# dzn ---------------------------------------------------------------------
#
# - 'Events' events : I = 0..('Events'-1)
#    event n°I at period EventPeriod_I into room EventRoom_I
# - 'Periods' periods : values 0..('Periods'-1)
# - 'Rooms' rooms : values 0..('Rooms'-1)
#
# Example toy.dzn case :
# - 13 events
# - 20 periods : values 0..19
# -  8 rooms : values 0..7 for A, B, C, G, H, {A,B}, {A,C}, dummy
#
# -------------------------------------------------------------------------
# data.py made from .dzn file by dzn2py.sh script
# -------------------------------------------------------------------------

from data import * # Events, Periods, Rooms, RoomedEvent, Conflicts,
                   # DistanceWeight, MinDistance, MaxDistance, Precedence,
                   # EventPeriodConstraints, EventRoomConstraints,
                   # RoomPeriodConstraints, RoomsetOverlap

def build_exam_tt_problem(version_name="PRPRPR_3", MAXCOST_VALUE=1000000) :
    """
    Versions (version_name) :
      - case not EEE : Variables : at least Period, Room (at least)
        - case PRPRPR_3 : ...
      - case EEE : Variables : Event

    MAXCOST_VALUE corresponds with -1 (impossible) cases of .mzn
    """

    AVAILABLE_VERSIONS = ["PRPRPR_4", "PRPRPR_3", "EEE"]

    if version_name not in AVAILABLE_VERSIONS :
        version_name = "PRPRPR_3" # default
        print("\nAvailable version names : ", str(AVAILABLE_VERSIONS))
    print("\nbuild_exam_tt_problem version ", version_name)

    top = MAXCOST_VALUE

    Problem = CFN(top)

    # Variables
    EventPeriod = ['P_'+str(e) for e in range(Events)] # P_0,P_1,...,P_12
    EventRoom =   ['R_'+str(e) for e in range(Events)] # R_0,R_1,...,R_12
    EventPeriodRoom = ['PR_'+str(e) for e in range(Events)]
                                                       # PR_0,PR_1,...,PR_12
    SamePeriod = [['s_' + str(e1) + '_' + str(e2) for e2 in range(Events)] for e1 in range(Events)]
    PeriodsRooms = Periods * Rooms

    if version_name == "EEE" :
        for e in range(Events):
            Problem.AddVariable(EventPeriodRoom[e], range(PeriodsRooms))
            #Problem.AddVariable(EventPeriodRoom[e],
            #    ['pr_' + str(p+1)+'_'+str(r+1) for p in range(Periods) for r in range(Rooms)])
    elif version_name == "PRPRPR_4" : 
        for e in range(Events):
            Problem.AddVariable(EventPeriod[e], range(Periods))
            Problem.AddVariable(EventRoom[e], range(Rooms))
    elif version_name == "PRPRPR_3" : 
        for e in range(Events):
            Problem.AddVariable(EventPeriod[e], range(Periods))
            Problem.AddVariable(EventRoom[e], range(Rooms))
        for e1 in range(Events-1):
          for e2 in range(e1+1, Events):
              Problem.AddVariable(SamePeriod[e1][e2], range(2))

    # Note SOFT_VS_HARD :
    # Soft constraints costs values must not decrease MAXCOST_VALUE costs
    # values due to hard conflicts. Be careful about -1 values into arrays.

    #--------------------------------------------------------------------------
    #                            SCOPE_P_P_R_R
    #--------------------------------------------------------------------------

    ########
    # HARD # NO_ROOM_OVERLAP
    ########

    # mzn ---------------------
    #
    # NO ROOM OVERLAP
    # notes: 
    #     - dummy room does not overlap with itself (more events can fit in
    #       the dummy room)
    #     - no constraint is added in the cases where the stronger one 
    #       EventPeriod[e1] != EventPeriod[e2] applies
    # constraint
    #   forall(e1 in 1..Events - 1, e2 in e1 + 1..Events 
    #           where Conflicts[e1, e2] != -1 /\
    #                 Precedence[e1, e2] = 0 /\
    #                 RoomedEvent[e1] = 1 /\ RoomedEvent[e2] = 1)
    #     (EventPeriod[e1] != EventPeriod[e2] \/ 
    #      RoomsetOverlap[EventRoom[e1], EventRoom[e2]] = 0);
    #
    # -------------------------

    # --> NOT : (EventPeriod[e1] == EventPeriod[e2]) /\
    #           (RoomsetOverlap[EventRoom[e1], EventRoom[e2]] != 0)
    #

    if version_name == "PRPRPR_4" :

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if (Conflicts[e1][e2] != -1) and \
                   (Precedence[e1][e2] == 0) and \
                   (RoomedEvent[e1] == 1) and (RoomedEvent[e2] == 1) :
                   # no hard conflict , no precedence , no dummy

                    scope = [EventPeriod[e1], EventPeriod[e2],
                             EventRoom[e1], EventRoom[e2]] # SCOPE_P_P_R_R

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            for r1 in range(Rooms) :
                                for r2 in range(Rooms) :
                                    if (p2 == p1) and \
                                       (RoomsetOverlap[r1][r2] != 0) :
                                        cost = MAXCOST_VALUE
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    elif version_name == "PRPRPR_3" :

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                scope = [SamePeriod[e1][e2], EventPeriod[e1], EventPeriod[e2]]
                # SCOPE_P_P_R_R => SCOPE_S_P_P & SCOPE_S_R_R

                costs = list()
                for s in range(2) :
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            if (p2 == p1) and (s == 0) :
                                cost = MAXCOST_VALUE
                            elif (p2 != p1) and (s == 1) :
                                cost = MAXCOST_VALUE
                            else :
                                cost = 0
                            costs.append(cost)

                Problem.AddFunction(scope, costs)

                if (Conflicts[e1][e2] != -1) and \
                   (Precedence[e1][e2] == 0) and \
                   (RoomedEvent[e1] == 1) and (RoomedEvent[e2] == 1) :
                   # no hard conflict , no precedence , no dummy

                    scope = [SamePeriod[e1][e2], EventRoom[e1], EventRoom[e2]]
                    # SCOPE_P_P_R_R => SCOPE_S_P_P & SCOPE_S_R_R

                    costs = list()
                    for s in range(2) :
                        for r1 in range(Rooms) :
                            for r2 in range(Rooms) :
                                if (s == 1) and (RoomsetOverlap[r1][r2] != 0) :
                                    cost = MAXCOST_VALUE
                                else :
                                    cost = 0
                                costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if (Conflicts[e1][e2] != -1) and \
                   (Precedence[e1][e2] == 0) and \
                   (RoomedEvent[e1] == 1) and (RoomedEvent[e2] == 1) :
                   # no hard conflict , no precedence , no dummy

                    # SCOPE_P_P_R_R => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    if (p2 == p1) and \
                                       (RoomsetOverlap[r1][r2] != 0) :
                                        cost = MAXCOST_VALUE
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    #--------------------------------------------------------------------------
    #                            SCOPE_P_P
    #--------------------------------------------------------------------------

    ########
    # HARD # HARD_CONFLICTS
    ########

    # mzn ---------------------
    #
    # HARD CONFLICTS
    # constraint
    #   forall(e1 in 1..Events - 1, e2 in e1+1..Events
    #                                           where Conflicts[e1, e2] = -1)
    #     (EventPeriod[e1] != EventPeriod[e2]);
    #
    # -------------------------

    if version_name != "EEE" :

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if Conflicts[e1][e2] == -1 :

                    scope = [EventPeriod[e1], EventPeriod[e2]] # SCOPE_P_P

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            if (p2 == p1) :
                                cost = MAXCOST_VALUE
                            else :
                                cost = 0
                            costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if Conflicts[e1][e2] == -1 :

                    # SCOPE_P_P => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    if (p2 == p1) :
                                        cost = MAXCOST_VALUE
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    ########
    # HARD # PRECEDENCES
    ########

    # mzn ---------------------
    #
    # PRECEDENCES
    # constraint
    #   forall(e1 in 1..Events - 1, e2 in e1 + 1..Events
    #                                           where Precedence[e1, e2] = 1)
    #     (EventPeriod[e1] < EventPeriod[e2]);
    #
    # -------------------------

    if version_name != "EEE" :

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if Precedence[e1][e2] == 1 :

                    scope = [EventPeriod[e1], EventPeriod[e2]] # SCOPE_P_P

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            if p1 >= p2 :
                                cost = MAXCOST_VALUE
                            else :
                                cost = 0
                            costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if Precedence[e1][e2] == 1 :

                    # SCOPE_P_P => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    if p1 >= p2 :
                                        cost = MAXCOST_VALUE
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    ########
    # SOFT # SOFT_CONFLICT_COSTS (ConflictCost)
    ########

    # Note SOFT_VS_HARD OK : SOFT_CONFLICT_COSTS assigns only values >= 0

    # mzn ---------------------
    #
    # SOFT CONFLICT COSTS
    # constraint
    # ConflictCost = sum(e1 in 1..Events - 1, e2 in e1 + 1..Events
    #                                             where Conflicts[e1, e2] > 0)
    #      (bool2int(EventPeriod[e1] = EventPeriod[e2]) *  Conflicts[e1, e2]);
    #
    # -------------------------

    if version_name != "EEE" :

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if Conflicts[e1][e2] > 0 :

                    scope = [EventPeriod[e1], EventPeriod[e2]] # SCOPE_P_P

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            if p2 == p1 :
                                cost = Conflicts[e1][e2]
                            else :
                                cost = 0
                            costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if Conflicts[e1][e2] > 0 :

                    # SCOPE_P_P => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    if p2 == p1 :
                                        cost = Conflicts[e1][e2]
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    ########
    # SOFT # DISTANCE_COSTS
    ########

    # mzn ---------------------
    #
    # DISTANCE COSTS (min and max, directional and undirectional)
    #
    # -------------------------

    # Note SOFT_VS_HARD OK : all DISTANCE_COSTS (MinDirectionalDistanceCost,
    # MaxDirectionalDistanceCost, MinUndirectionalDistanceCost,
    # MaxUndirectionalDistanceCost) assign only values >= 0

    ########
    # SOFT # DISTANCE_COSTS (MinDirectionalDistanceCost)
    ########

    # mzn ---------------------
    #
    # constraint
    # MinDirectionalDistanceCost = sum(e1 in 1..Events, e2 in 1..Events 
    #        where Precedence[e1, e2] = 1 /\
    #              DistanceWeight[e1, e2] > 0 /\ MinDistance[e1, e2] > 0)
    #  (DistanceWeight[e1, e2] *
    #  bool2int(EventPeriod[e2] - EventPeriod[e1] < MinDistance[e1, e2]) *
    #  (MinDistance[e1, e2] - (EventPeriod[e2] - EventPeriod[e1])));
    #
    # -------------------------

    if version_name != "EEE" :

        for e1 in range(Events) :
            for e2 in range(Events) :

                if (Precedence[e1][e2] == 1) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MinDistance[e1][e2] > 0) :

                    scope = [EventPeriod[e1], EventPeriod[e2]] # SCOPE_P_P

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            delta_p = p2 - p1
                            if (delta_p < MinDistance[e1][e2]) :
                                cost = DistanceWeight[e1][e2] * \
                                              (MinDistance[e1][e2] - delta_p)
                            else :
                                cost = 0
                            costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events) :
            for e2 in range(Events) :

                if (Precedence[e1][e2] == 1) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MinDistance[e1][e2] > 0) :

                    # SCOPE_P_P => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    delta_p = p2 - p1
                                    if (delta_p < MinDistance[e1][e2]) :
                                        cost = DistanceWeight[e1][e2] * \
                                              (MinDistance[e1][e2] - delta_p)
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    ########
    # SOFT # DISTANCE_COSTS (MaxDirectionalDistanceCost)
    ########

    # mzn ---------------------
    #
    # constraint
    # MaxDirectionalDistanceCost = sum(e1 in 1..Events, e2 in 1..Events 
    #        where Precedence[e1, e2] = 1 /\
    #              DistanceWeight[e1, e2] > 0 /\ MaxDistance[e1, e2] < Periods)
    #  (DistanceWeight[e1, e2] *
    #  bool2int(EventPeriod[e2] - EventPeriod[e1] > MaxDistance[e1, e2]) *
    #  (EventPeriod[e2] - EventPeriod[e1] - MaxDistance[e1, e2]));
    #
    # -------------------------

    if version_name != "EEE" :

        for e1 in range(Events) :
            for e2 in range(Events) :

                if (Precedence[e1][e2] == 1) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MaxDistance[e1][e2] < Periods) :

                    scope = [EventPeriod[e1], EventPeriod[e2]] # SCOPE_P_P

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            delta_p = p2 - p1
                            if delta_p > MaxDistance[e1][e2] :
                                cost = DistanceWeight[e1][e2] * \
                                              (delta_p - MaxDistance[e1][e2])
                            else :
                                cost = 0
                            costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events) :
            for e2 in range(Events) :

                if (Precedence[e1][e2] == 1) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MaxDistance[e1][e2] < Periods) :

                    # SCOPE_P_P => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    delta_p = p2 - p1
                                    if delta_p > MaxDistance[e1][e2] :
                                        cost = DistanceWeight[e1][e2] * \
                                              (delta_p - MaxDistance[e1][e2])
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    ########
    # SOFT # DISTANCE_COSTS (MinUndirectionalDistanceCost)
    ########

    # mzn ---------------------
    #
    # constraint
    # MinUndirectionalDistanceCost = sum(e1 in 1..Events - 1, e2 in e1+1..Events
    #          where Precedence[e1, e2] = 0 /\
    #                DistanceWeight[e1, e2] > 0 /\ MinDistance[e1, e2] > 0)
    #  (DistanceWeight[e1, e2] *
    #  bool2int(abs(EventPeriod[e2] - EventPeriod[e1]) < MinDistance[e1, e2]) *
    #  (MinDistance[e1, e2] - abs(EventPeriod[e2] - EventPeriod[e1])));
    #
    # -------------------------

    if version_name != "EEE" :

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if (Precedence[e1][e2] == 0) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MinDistance[e1][e2] > 0) :

                    scope = [EventPeriod[e1], EventPeriod[e2]] # SCOPE_P_P

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            absdelta_p = abs(p2 - p1)
                            if absdelta_p < MinDistance[e1][e2] :
                                cost = DistanceWeight[e1][e2] * \
                                           (MinDistance[e1][e2] - absdelta_p)
                            else :
                                cost = 0
                            costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if (Precedence[e1][e2] == 0) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MinDistance[e1][e2] > 0) :

                    # SCOPE_P_P => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    absdelta_p = abs(p2 - p1)
                                    if absdelta_p < MinDistance[e1][e2] :
                                        cost = DistanceWeight[e1][e2] * \
                                           (MinDistance[e1][e2] - absdelta_p)
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    ########
    # SOFT # DISTANCE_COSTS (MaxUndirectionalDistanceCost)
    ########

    # mzn ---------------------
    #
    # constraint
    # MaxUndirectionalDistanceCost = sum(e1 in 1..Events - 1, e2 in e1+1..Events
    #          where Precedence[e1, e2] = 0 /\
    #                DistanceWeight[e1, e2] > 0 /\
    #                MaxDistance[e1, e2] < Periods)
    #  (DistanceWeight[e1, e2] *
    #  bool2int(abs(EventPeriod[e2] - EventPeriod[e1]) > MaxDistance[e1, e2]) *
    #  (abs(EventPeriod[e2] - EventPeriod[e1]) - MaxDistance[e1, e2]));
    #
    # -------------------------

    if version_name != "EEE" :

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if (Precedence[e1][e2] == 0) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MaxDistance[e1][e2] < Periods) :

                    scope = [EventPeriod[e1], EventPeriod[e2]] # SCOPE_P_P

                    costs = list()
                    for p1 in range(Periods) :
                        for p2 in range(Periods) :
                            absdelta_p = abs(p2 - p1)
                            if absdelta_p > MaxDistance[e1][e2] :
                                cost = DistanceWeight[e1][e2] * \
                                           (absdelta_p - MaxDistance[e1][e2])
                            else :
                                cost = 0
                            costs.append(cost)

                    Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e1 in range(Events-1) :
            for e2 in range(e1+1, Events):

                if (Precedence[e1][e2] == 0) and \
                   (DistanceWeight[e1][e2] > 0) and \
                   (MaxDistance[e1][e2] < Periods) :

                    # SCOPE_P_P => SCOPE_PR_PR
                    scope = [EventPeriodRoom[e1], EventPeriodRoom[e2]]

                    costs = list()
                    for p1 in range(Periods) :
                        for r1 in range(Rooms) :
                            for p2 in range(Periods) :
                                for r2 in range(Rooms) :
                                    absdelta_p = abs(p2 - p1)
                                    if absdelta_p > MaxDistance[e1][e2] :
                                        cost = DistanceWeight[e1][e2] * \
                                           (absdelta_p - MaxDistance[e1][e2])
                                    else :
                                        cost = 0
                                    costs.append(cost)

                    Problem.AddFunction(scope, costs)

    #--------------------------------------------------------------------------
    #                            SCOPE_P_R
    #--------------------------------------------------------------------------

    ########
    # HARD # ASSIGNED_ROOM_SHOULD_BE_AVAILABLE_AT_ASSIGNED_PERIOD
    # SOFT # PERIOD_DEPENDENT_UNDESIRED_ROOMS (RoomPeriodCost)
    ########

    # ASSIGNED_ROOM_SHOULD_BE_AVAILABLE_AT_ASSIGNED_PERIOD and
    # PERIOD_DEPENDENT_UNDESIRED_ROOMS gathered

    # Note SOFT_VS_HARD OK : The -1 values of RoomPeriodConstraints (forbidden
    # cases) do not appear into toulbar2 because modified as MAXCOST_VALUE.

    # Note : The forbidden cases (-1 values of RoomPeriodConstraints) would
    # contribute as -1 into RoomPeriodCost of MiniZinc if they happened, but
    # they do not happen (cf hard constraint).

    # mzn ---------------------
    #
    # ASSIGNED ROOM SHOULD BE AVAILABLE AT ASSIGNED PERIOD
    # constraint
    #   forall(e in 1..Events, p in 1..Periods) 
    #     (RoomPeriodConstraints[EventRoom[e], EventPeriod[e]] != -1);
    #
    # -------------------------
    #
    # PERIOD DEPENDENT UNDESIRED ROOMS
    # constraint 
    #   RoomPeriodCost = sum(e in 1..Events)(RoomPeriodConstraints[EventRoom[e],
    #                                                         EventPeriod[e]]);
    # -------------------------

    if version_name != "EEE" :

        for e in range(Events) : 

            scope = [EventPeriod[e], EventRoom[e]] # SCOPE_P_R

            costs = list()
            for p in range(Periods) :
                for r in range(Rooms) :
                    if RoomPeriodConstraints[r][p] == -1 :
                        cost = MAXCOST_VALUE
                        # ASSIGNED_ROOM_SHOULD_BE_AVAILABLE_AT_ASSIGNED_PERIOD
                    else :
                        cost = RoomPeriodConstraints[r][p]
                                            # PERIOD_DEPENDENT_UNDESIRED_ROOMS
                    costs.append(cost)

            Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e in range(Events) : 

            scope = [EventPeriodRoom[e]] # SCOPE_P_R => SCOPE_PR

            costs = list()
            for p in range(Periods) :
                for r in range(Rooms) :
                    if RoomPeriodConstraints[r][p] == -1 :
                        cost = MAXCOST_VALUE
                         # ASSIGNED_ROOM_SHOULD_BE_AVAILABLE_AT_ASSIGNED_PERIOD
                    else :
                        cost = RoomPeriodConstraints[r][p]
                                            # PERIOD_DEPENDENT_UNDESIRED_ROOMS
                    costs.append(cost)

            Problem.AddFunction(scope, costs)

    #--------------------------------------------------------------------------
    #                            SCOPE_P
    #--------------------------------------------------------------------------

    ########
    # HARD # FORBIDDEN_PERIOD_CONSTRAINTS
    # SOFT # PERIOD_PREFERENCE_COST (PeriodPreferenceCost)
    ########

    # FORBIDDEN_PERIOD_CONSTRAINTS and
    # PERIOD_PREFERENCE_COST (PeriodPreferenceCost) gathered

    # Note SOFT_VS_HARD OK : The -1 values of EventPeriodConstraints
    # (forbidden cases) do not appear into toulbar2 because modified as
    # MAXCOST_VALUE.

    # Note : The forbidden cases (-1 values of EventPeriodConstraints) would
    # contribute as -1 into PeriodPreferenceCost of MiniZinc if they happened,
    # but they do not happen (cf hard constraint).

    # mzn ---------------------
    #
    # FORBIDDEN PERIOD CONSTRAINTS
    # constraint
    #   forall(e in 1..Events, p in 1..Periods 
    #                                where EventPeriodConstraints[e, p] = -1) 
    #     (EventPeriod[e] != p);
    #
    # -------------------------
    #
    # PERIOD PREFERENCE COST
    # constraint
    #    PeriodPreferenceCost = sum(e in 1..Events)
    #                            (EventPeriodConstraints[e, EventPeriod[e]]);
    #
    # -------------------------

    if version_name != "EEE" :

        for e in range(Events) :

            scope = [EventPeriod[e]] # SCOPE_P

            costs = [0 for p in range(Periods)]
            for p in range(Periods) :
                if EventPeriodConstraints[e][p] == -1 :
                    costs[p] = MAXCOST_VALUE # FORBIDDEN_PERIOD_CONSTRAINTS
                else :
                    costs[p] = EventPeriodConstraints[e][p]
                                                      # PERIOD_PREFERENCE_COST

            Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e in range(Events) :

            scope = [EventPeriodRoom[e]] # SCOPE_P => SCOPE_PR

            costs = [0 for p in range(PeriodsRooms)]
            c = 0
            for p in range(Periods) :
                for r in range(Rooms) :
                    if EventPeriodConstraints[e][p] == -1 :
                        costs[c] = MAXCOST_VALUE # FORBIDDEN_PERIOD_CONSTRAINTS
                    else :
                        costs[c] = EventPeriodConstraints[e][p]
                                                      # PERIOD_PREFERENCE_COST
                    c = c + 1

            Problem.AddFunction(scope, costs)

    #--------------------------------------------------------------------------
    #                            SCOPE_R
    #--------------------------------------------------------------------------

    ########
    # HARD # FORBIDDEN_ROOMS_CONSTRAINTS
    # SOFT # ROOM_PREFERENCE_COST (RoomPreferenceCost)
    ########

    # FORBIDDEN_ROOMS_CONSTRAINTS and ROOM_PREFERENCE_COST gathered

    # Note SOFT_VS_HARD OK : The -1 values of EventRoomConstraints (forbidden
    # cases) do not appear into toulbar2 because modified as MAXCOST_VALUE.

    # Note : The forbidden cases (-1 values of EventRoomConstraints) would
    # contribute as -1 into RoomPreferenceCost of MiniZinc if they happened,
    # but they do not happen (cf hard constraint).

    # mzn ---------------------
    #
    # FORBIDDEN ROOMS CONSTRAINTS
    # constraint
    #   forall(e in 1..Events, r in 1..Rooms 
    #                                  where EventRoomConstraints[e, r] = -1) 
    #     (EventRoom[e] != r);
    #
    # -------------------------
    #
    # ROOM PREFERENCE COST
    # constraint
    #    RoomPreferenceCost = sum(e in 1..Events)
    #                                (EventRoomConstraints[e, EventRoom[e]]);
    #
    # -------------------------

    if version_name != "EEE" :

        for e in range(Events) :

            scope = [EventRoom[e]] # SCOPE_R

            costs = [0 for r in range(Rooms)]

            for r in range(Rooms) :
                if EventRoomConstraints[e][r] == -1 :
                    costs[r] = MAXCOST_VALUE
                else :
                    costs[r] = EventRoomConstraints[e][r]

            Problem.AddFunction(scope, costs)

    else : # "EEE" version

        for e in range(Events) :

            scope = [EventPeriodRoom[e]] # SCOPE_R => SCOPE_PR

            costs = [0 for p in range(PeriodsRooms)]

            c = 0
            for p in range(Periods) :
                for r in range(Rooms) :
                    if EventRoomConstraints[e][r] == -1 :
                        costs[c] = MAXCOST_VALUE
                    else :
                        costs[c] = EventRoomConstraints[e][r]
                    c = c + 1

            Problem.AddFunction(scope, costs)

    return Problem

