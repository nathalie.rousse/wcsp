###############################################################################
# 
#                     jeux_ref
#
###############################################################################

#------------------------------------------------------------------------------
# Desc

Jeux de donnees servant de reference pour comparer des codes du model/problem,
considerer qu'ils sont identiques.

#------------------------------------------------------------------------------
# JEU toybis

Desc :

   Une variante de toy qui a ete modifie pour qu'il y ait une solution unique.

Ecriture :

   Creation de toybis.py a partir toy.py, modifications.
   toybis.py a ensuite ete converti en toybis.dzn pour cote minizinc.

Runs

- toulbar2 :

  voir report_002

  1) pytoulbar2 pour PRPRPR_3 PRPRPR_4 EEE

  => Sauvegardes dans source/data des problems toulbar2 (.wcsp, .cfn) :
     toybis__PRPRPR_3.wcsp  toybis__PRPRPR_3.cfn
     toybis__PRPRPR_4.wcsp  toybis__PRPRPR_4.cfn
     toybis__EEE.wcsp       toybis__EEE.cfn

  2) toulbar2 pour
     toybis__PRPRPR_3.wcsp  toybis__PRPRPR_3.cfn
     toybis__PRPRPR_4.wcsp  toybis__PRPRPR_4.cfn
     toybis__EEE.wcsp       toybis__EEE.cfn

- minizinc :

  voir report_004

  minizinc pour exam_tt.mzn toybis.dzn avec solvers gecode chuffed et coin-bc

#------------------------------------------------------------------------------

