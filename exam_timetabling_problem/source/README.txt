###############################################################################
#
#                     Modele emploi du temps
#
#                  Examination Timetabling model
#
#                 (exam_tt.mzn + .dzn with Minizinc)
#
###############################################################################

###############################################################################
# Reference
###############################################################################

CPAIOR 2020
"Local Search and Constraint Programming for a Real-World Examination Timetabling Problem"
Michele Battistutta, Sara Ceschia, Fabio De Cesco, Luca Di Gaspero, Andrea Schaerf and Elena Topan

Instances, solutions, and the MiniZinc model at :
https://bitbucket.org/satt/ExamTimetablingUniudData

###############################################################################
# Minizinc source (code and data)
###############################################################################

- To get CPAIOR2020 data and exam_tt.mzn model :

  git clone https://bitbucket.org/satt/examtimetablinguniuddata.git
  rm -fr examtimetablinguniuddata/Revised
  rm -fr examtimetablinguniuddata/toolbox_package

- Content :

  examtimetablinguniuddata/exam_tt.mzn
  examtimetablinguniuddata/CPAIOR2020/InstancesDZN/*.dzn
  examtimetablinguniuddata/CPAIOR2020/InstancesJSON/*.json
  examtimetablinguniuddata/CPAIOR2020/Solutions/sol_*.json

- PATH of *.dzn data files : examtimetablinguniuddata/CPAIOR2020/InstancesDZN

###############################################################################

