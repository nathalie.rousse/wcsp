# ExamTimetablingUniud Repository

This repository contains the CP model and the instances for the problem presented in the paper *Local search and constraint programming for a real-world examination timetabling problem* by Michele Battistutta, Sara Ceschia, Fabio De Cesco, Luca Di Gaspero, Andrea Schaerf and Elena Topan (submitted for publication). It includes also the best solutions obtained by the local search method described in the paper, and the solution validator.

## Constraint programming model

The constraint programming [model](exam_tt.mzn) has been formulated in [MiniZinc](https://www.minizinc.org).

## Dataset

The instances are available in *JSON* format in the folder [`InstancesJSON`](InstancesJSON) and 
in *DZN* format in the folder [`InstancesDZN`](InstancesDZN) for the MiniZinc model.

The dataset collects 40 real world instances coming from 7 different departments (of 6 different universities), and the toy instance described in the paper.

### Edit February 23rd, 2021

**The DataZinc format has been updated to include further information.**


## Validator

A Python validator for this formulation is available as a companion repository at [https://bitbucket.org/satt/examtt-uniud-validator](https://bitbucket.org/satt/examtt-uniud-validator). The validator checks the feasibility and the cost of a solution, so as to provide against possible misunderstanding about the constraints and the objectives. 

## Solutions

Best solutions are available in the folder [`Solutions`](Solutions) in JSON format.

