#!/bin/bash

#------------------------------------------------------------------------------

ROOT_PATH=/home/nrousse/workspace_git/wcsp/exam_timetabling_problem

#------------------------------------------------------------------------------
# Source

# for example, toy.dzn
DATA_DZN_PATH=${ROOT_PATH}/source/examtimetablinguniuddata/CPAIOR2020/InstancesDZN

# for example, toy.json
DATA_JSON_PATH=${ROOT_PATH}/source/examtimetablinguniuddata/CPAIOR2020/InstancesJSON

# for example, sol_toy.json
DATA_SOL_PATH=${ROOT_PATH}/source/examtimetablinguniuddata/CPAIOR2020/Solutions

export DATA_DZN_PATH
export DATA_JSON_PATH
export DATA_SOL_PATH

#------------------------------------------------------------------------------
# Solution

DATA_SOL_JSON_PATH=${DATA_SOL_PATH}
DATA_SOL_BRUT_PATH=${ROOT_PATH}/data_more/SolutionsBrut
export DATA_SOL_JSON_PATH
export DATA_SOL_BRUT_PATH

#------------------------------------------------------------------------------
# For python code

# for example, toy.py (built from toy.dzn)
DATA_PY_PATH=${ROOT_PATH}/data_more/CPAIOR2020_InstancesPY

# for example, toyJSON.py (built from toy.json)
DATA_JSONPY_PATH=${ROOT_PATH}/data_more/CPAIOR2020_InstancesJSONPY

export DATA_PY_PATH
export DATA_JSONPY_PATH

#------------------------------------------------------------------------------

JEUX="D1-1-16 D1-1-17 D1-2-16 D1-2-17 D1-3-16 D1-3-17 D1-3-18 D2-1-18 D2-2-18 D2-3-18 D3-1-16 D3-1-17 D3-1-18 D3-2-16 D3-2-17 D3-2-18 D3-3-16 D3-3-17 D3-3-18 D4-1-17 D4-1-18 D4-2-17 D4-2-18 D4-3-17 D4-3-18 D5-1-17 D5-1-18 D5-2-17 D5-2-18 D5-3-18 D6-1-16 D6-1-17 D6-1-18 D6-2-16 D6-2-17 D6-2-18 D6-3-16 D6-3-17 D7-1-17 D7-2-17 toy"
export JEUX

JEUX_SEL="D2-1-18 D2-2-18 D2-3-18 D3-2-16 D3-2-17 D3-2-18 D3-3-16 D3-3-17 D3-3-18 D5-3-18 toy"
export JEUX_SEL

#------------------------------------------------------------------------------
# exam_tt.py pytoulbar2 problem

VERSION_NAMES="EEE PRPRPR_4 PRPRPR_3"
export VERSION_NAMES

#------------------------------------------------------------------------------

