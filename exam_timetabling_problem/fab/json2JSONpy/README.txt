###############################################################################
#
#                json2JSONpy

 
#
###############################################################################

Produced data files :

  ${DATA_JSON_PATH}/toy.json --> ${DATA_JSONPY_PATH}/toyJSON.py

The resulting *.py data files are ready to be imported by .py code

###############################################################################

# generation for each $JEUX :

source ../../SETTINGS.sh
for JEU in `echo $JEUX`
do
  srcfile=${DATA_JSON_PATH}/${JEU}.json
  destfile=${DATA_JSONPY_PATH}/${JEU}JSON.py
  echo "dataJSON = \\" > ${destfile}
  cat ${srcfile} >> ${destfile}
  sed -i 's/false/False/g' ${destfile}
  sed -i 's/true/True/g' ${destfile}
done

