###############################################################################
#
#                       dzn2py tool
#
###############################################################################

# dzn2py/dzn2py.sh,dzn2py_control.py :

  - dzn2py.sh : 

    - makes .py data files from .dzn data files

    - controls .py produced files, by calling dzn2py_control.py code

  - The resulting data.py is ready to be imported by exam_tt.py

- Produced data files :

  ${DATA_DZN_PATH}/*.dzn ---- dzn2py.sh ----> ${DATA_PY_PATH}/*.py

###############################################################################

