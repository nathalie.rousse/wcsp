#!/bin/bash

ROOT=/home/nrousse/workspace_git/wcsp/exam_timetabling_problem
HOME=$ROOT/toulbar2/dzn2py
SOURCE=$ROOT/source/examtimetablinguniuddata/CPAIOR2020/InstancesDZN
DESTINATION=$ROOT/toulbar2/CPAIOR2020_InstancesPY

printf "\n"
printf "*** Generation of .py data files from .dzn data files ***" 
printf "\n"
if [ -e "$DESTINATION" ]; then
    printf "$DESTINATION already exists -> STOP \n";
    exit;
fi

mkdir $DESTINATION

# .dzn -> .py
printf "\n"
printf "Generates"
cd $SOURCE
for f in *.dzn; do
  printf " .."
  sed 's/\[|/[\[/' $f > $DESTINATION/tmp1;
  sed 's/|\]/]\]/' $DESTINATION/tmp1 > $DESTINATION/tmp2;
  sed 's/|/],\[/' $DESTINATION/tmp2 > $DESTINATION/$f.py;
done

# rename py files
cd $DESTINATION
for f in *.py; do fn=`echo $f|sed 's/.dzn//'`; mv "$f" "$fn"; done

printf "\n\n"
printf "Controls ..."
printf "\n"
cd $HOME
for f in $DESTINATION/*.py; do
  rm -f data.py ;
  ln -s $f data.py ;
  python dzn2py_control.py ;
done

printf "\n"
printf "End (OK if no message)"
printf "\n"

