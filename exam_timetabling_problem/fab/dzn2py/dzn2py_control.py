###############################################################################
#
# Controls data.py content :
#
# - python syntax
# - size of lists as required (cf Events, Periods, Rooms)
#
# source ENV.sh !!!
###############################################################################

import os

from data import * # Events, Periods, Rooms, RoomedEvent, Conflicts,
                   # DistanceWeight, MinDistance, MaxDistance, Precedence,
                   # EventPeriodConstraints, EventRoomConstraints,
                   # RoomPeriodConstraints, RoomsetOverlap

cr_ok = True
msg = str()

def control_list(l, name, size, msg) :
    """Controls list l (named name) : type and size """

    cr = True
    if type(l) != type(list()) :
        cr = False
        msg += "Type of " +name+ " " +str(type(l))+ " is not list" + "\n"
    elif (len(l) != size) :
        cr = False
        msg += "Size of " +name+ " " +str(len(l))+ " != " +str(size)+ "\n"
    return (cr, msg)

(cr, msg) = control_list(RoomedEvent, "RoomedEvent", Events, msg)
if not cr :
    cr_ok = False

(cr, msg) = control_list(Conflicts, "Conflicts", Events, msg)
if not cr :
    cr_ok = False

(cr, msg) = control_list(DistanceWeight, "DistanceWeight", Events, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(DistanceWeight) :
        name = "DistanceWeight/line_"+str(i)
        (cr, msg) = control_list(line, name, Events, msg)
        if not cr :
            cr_ok = False

(cr, msg) = control_list(MinDistance, "MinDistance", Events, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(MinDistance) :
        name = "MinDistance/line_"+str(i)
        (cr, msg) = control_list(line, name, Events, msg)
        if not cr :
            cr_ok = False

(cr, msg) = control_list(MaxDistance, "MaxDistance", Events, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(MaxDistance) :
        name = "MaxDistance/line_"+str(i)
        (cr, msg) = control_list(line, name, Events, msg)
        if not cr :
            cr_ok = False

(cr, msg) = control_list(Precedence, "Precedence", Events, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(Precedence) :
        name = "Precedence/line_"+str(i)
        (cr, msg) = control_list(line, name, Events, msg)
        if not cr :
            cr_ok = False

(cr, msg) = control_list(EventPeriodConstraints, "EventPeriodConstraints",
                         Events, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(EventPeriodConstraints) :
        name = "EventPeriodConstraints/line_"+str(i)
        (cr, msg) = control_list(line, name, Periods, msg)
        if not cr :
            cr_ok = False

(cr, msg) = control_list(EventRoomConstraints, "EventRoomConstraints",
                         Events, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(EventRoomConstraints) :
        name = "EventRoomConstraints/line_"+str(i)
        (cr, msg) = control_list(line, name, Rooms, msg)
        if not cr :
            cr_ok = False

(cr, msg) = control_list(RoomPeriodConstraints, "RoomPeriodConstraints",
                         Rooms, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(RoomPeriodConstraints) :
        name = "RoomPeriodConstraints/line_"+str(i)
        (cr, msg) = control_list(line, name, Periods, msg)
        if not cr :
            cr_ok = False

(cr, msg) = control_list(RoomsetOverlap, "RoomsetOverlap", Rooms, msg)
if not cr :
    cr_ok = False
else :
    for i, line in enumerate(RoomsetOverlap) :
        name = "RoomsetOverlap/line_"+str(i)
        (cr, msg) = control_list(line, name, Rooms, msg)
        if not cr :
            cr_ok = False

if cr_ok :
    #print(os.readlink("data.py"))
    #print("    OK")
    pass
else : 
    print(os.readlink("data.py"))
    print("NOT OK :")
    print(msg)

if False : # True :
    print("--------- DATA ---------")
    print("Events=", Events, " ; ",
          "Periods=", Periods, " ; ", "Rooms=", Rooms)
    print("RoomedEvent=", RoomedEvent)
    print("Conflicts=", Conflicts)
    print("DistanceWeight=", DistanceWeight)
    print("MinDistance=", MinDistance)
    print("MaxDistance=", MaxDistance)
    print("Precedence=", Precedence)
    print("EventPeriodConstraints=", EventPeriodConstraints)
    print("EventRoomConstraints=", EventRoomConstraints)
    print("RoomPeriodConstraints=", RoomPeriodConstraints)
    print("RoomsetOverlap=", RoomsetOverlap)
    print("------------------------")

# -----------------------------------------------------------------------------
