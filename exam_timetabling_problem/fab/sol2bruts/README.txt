###############################################################################
#
#                             sol2solbrut
#
###############################################################################

Produced data files :

  ${DATA_SOL_JSON_PATH}/sol_toy.json --> ${DATA_SOL_BRUT_PATH}/solbrut_toy.txt
  ${DATA_JSONPY_PATH}/toy.json 

The resulting *.txt data files aim to be compared with JEU_brut_result.txt from pytoulbar2

###############################################################################

# generation for each $JEUX :

source ../../SETTINGS.sh

for JEU in `echo $JEUX`
do

  srcfile=${DATA_SOL_JSON_PATH}/sol_${JEU}.json
  echo "solutionJSON = \\" > solutionJSON.py
  cat ${srcfile} >> solutionJSON.py

  cat ${DATA_JSONPY_PATH}/${JEU}JSON.py > dataJSON.py

  destfile=${DATA_SOL_BRUT_PATH}/solbrut_${JEU}.txt
  python3 solutions.py ${destfile}

done



