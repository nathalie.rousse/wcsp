###############################################################################
#
# Reads solutionJSON.py (from sol_${JEU}.json) and dataJSON.py
# Generates destfile file + some print
#
###############################################################################

import sys

from solutionJSON import solutionJSON 
from dataJSON import dataJSON # for rooms names interpretation

destfile = None
if len(sys.argv) > 1 :
    destfile = sys.argv[1]
print("\nGenerate", destfile, "file ...")

print("\nFrom :")

# rooms_ind 
rooms_ind = dict()
RoomsJSON = dataJSON["Rooms"]
RoomsJSON.append({'Room': 'dummy', 'Type': 'dummy'})
for (i,r) in enumerate(RoomsJSON) :
    rooms_ind[r['Room']] = i
print("\nrooms_ind :", rooms_ind)
print("\nsolutionJSON :", solutionJSON)
print("solutionJSON.keys() :", solutionJSON.keys())

print("\nFound :")

s = list()

for key,value in solutionJSON.items() :

    if key == 'Assignments' :

        print("\nEvents list :")
        for assign in solutionJSON['Assignments'] :
            if 'Events' in assign.keys() :
                for event in assign['Events'] :
                    ekeys = event.keys()
                    if 'Period' in ekeys :
                        s.append(event['Period'])
                    else :
                        print("Error !!!! no 'Period' key for Event :", event)
                    if 'Room' in ekeys :
                        name = event['Room']
                    else :
                        name = 'dummy'
                    s.append(rooms_ind[name])
                    print("  ", event)

    #elif key == 'Cost' :
    #    print("\nCost : ", value)

    else :
        print("!!! Key '", key, "' into solutionJSON with value :", value)

try : # fills destfile
    fichier = open(destfile, "w")
    fichier.write(str(s))
    fichier.close()
except :
    pass

print("\n=> Generated into ", destfile, " file :")
print(s)

print("\nEnd.\n")






