###############################################################################
#
# Development with Minizinc
#
###############################################################################

exam_tt.mzn
===========

  exam_tt.mzn is the original model

  = source/examtimetablinguniuddata/exam_tt.mzn

exam_tt_distances_v1.mzn
========================

  exam_tt_distances_v1.mzn comes from exam_tt.mzn, modified so :

    - MinDirectionalDistanceCost and MinUndirectionalDistanceCost gathered in
      MinDISTANCECost
    - MaxDirectionalDistanceCost and MaxUndirectionalDistanceCost gathered in
      MaxDISTANCECost

###############################################################################

