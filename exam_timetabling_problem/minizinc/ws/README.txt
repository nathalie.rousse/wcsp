###############################################################################
# 
#                     ws run minizinc exam_tt.mzn
#
###############################################################################

source SETTINGS.sh
MINIZINC_PATH=${ROOT_PATH}/minizinc
MINIZINC_WS_PATH=${MINIZINC_PATH}/ws

#------------------------------------------------------------------------------
Creation :

    mkdir input
    cp ${MINIZINC_PATH}/exam_tt.mzn input/.
    cp ${MINIZINC_WS_PATH}/exam_tt_cmd.sh input/.

#------------------------------------------------------------------------------
# ws run :
# 1. construire input.zip
# 2. requete run ou muse/run

BASE_WS_URL=http://147.100.179.250

TOKENVALUE=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6Indzc3UiLCJleHAiOjE2MzI3NTkxOTUsImVtYWlsIjoiIn0.zXt6In8kNQDKSv8zpMA8YJrGRhw3WYOD8e5Ip3E4Kyo

echo "DATA_DZN_PATH=  $DATA_DZN_PATH"
echo "BASE_WS_URL = $BASE_WS_URL"
echo "JEUX_SEL = $JEUX_SEL"

for JEU in `echo $JEUX_SEL`
do
    echo "$JEU" > input/JEU.txt ; rm -f input.zip ; rm -f input/*.dzn ; cp ${DATA_DZN_PATH}/${JEU}.dzn input/. ; zip -r input.zip input
    curl --output _ws_${JEU}_run.zip -F 'jwt='${TOKENVALUE} -F 'file=@input.zip' -F 'cmd=/bin/bash input/exam_tt_cmd.sh '${JEU} -F 'returned_type=run.zip' -F 'todownload=no' ${BASE_WS_URL}/api/software/run/minizinc/

done

ou

echo $JEUX_SEL
for JEU in `echo $JEUX_SEL`
do
    echo "$JEU" > input/JEU.txt ; rm -f input.zip ; rm -f input/*.dzn ; cp ${DATA_DZN_PATH}/${JEU}.dzn input/. ; zip -r input.zip input
    curl --output _ws_${JEU}_muse_run.txt -F 'jwt='${TOKENVALUE} -F 'file=@input.zip' -F 'cmd=/bin/bash input/exam_tt_cmd.sh '${JEU} -F 'returned_type=run.zip' -F 'todownload=no' ${BASE_WS_URL}/api/software/muse/run/minizinc/

done

#------------------------------------------------------------------------------

