#!/bin/bash

#####################################################
# Init to run pytoulbar2 and toulbar2
#
# => 'source ENV.sh'
#
#####################################################

PATH=$PATH:/home/nrousse/workspace_git/toulbar2/build/bin/Linux:/home/nrousse/workspace_git/toulbar2/src
export PATH

PYTHONPATH=$PYTHONPATH:/home/nrousse/workspace_git/toulbar2/pytoulbar2
export PYTHONPATH

echo "PATH = " $PATH
echo "PYTHONPATH = " $PYTHONPATH

#####################################################

