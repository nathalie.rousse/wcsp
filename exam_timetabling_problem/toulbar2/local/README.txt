###############################################################################
#
#                          local run of exam_tt_main.py
#
# With ENV.sh and pytoulbar2 installed or 
# With python virtual env (see install.txt)
#
###############################################################################

source SETTINGS.sh

LOCAL_PATH=${ROOT_PATH}/toulbar2/local

# if with ENV.sh
source ${LOCAL_PATH}/ENV.sh

# if With python virtual env
source ${LOCAL_PATH}/pyvenv/pytoulbar2env/bin/activate

# VERSION_NAME available values : $VERSION_NAMES :
VERSION_NAME=EEE

for JEU in `echo $JEUX`
do
done

JEU=toy
echo "$JEU" > JEU.txt ; rm -f data.py ; rm -f dataJSON.py ; rm -f exam_tt_main.py ; rm -f exam_tt_pb.py ; rm -f measure.py ; rm -f result.py ; rm -f compare.py ; ln -s ${DATA_PY_PATH}/${JEU}.py data.py ; ln -s ${DATA_JSONPY_PATH}/${JEU}JSON.py dataJSON.py ; cp ${ROOT_PATH}/toulbar2/exam_tt_main.py exam_tt_main.py ; cp ${ROOT_PATH}/toulbar2/exam_tt_pb.py exam_tt_pb.py ; cp ${ROOT_PATH}/toulbar2/measure.py measure.py ; cp ${ROOT_PATH}/toulbar2/result.py result.py ; cp ${ROOT_PATH}/toulbar2/compare.py compare.py ; python exam_tt_main.py $VERSION_NAME

###############################################################################

Choose VERSION_NAME in `echo $VERSION_NAMES` :
VERSION_NAME=EEE
VERSION_NAME=PRPRPR_4
VERSION_NAME=PRPRPR_3

mkdir 002_${VERSION_NAME}
cd 002_${VERSION_NAME}

echo "$JEU" > JEU.txt ; rm -f data.py ; rm -f dataJSON.py ; rm -f exam_tt_main.py ; rm -f exam_tt_pb.py ; rm -f measure.py ; rm -f result.py ; rm -f compare.py ; ln -s ${DATA_PY_PATH}/${JEU}.py data.py ; ln -s ${DATA_JSONPY_PATH}/${JEU}JSON.py dataJSON.py ; cp ${ROOT_PATH}/toulbar2/exam_tt_main.py exam_tt_main.py ; cp ${ROOT_PATH}/toulbar2/exam_tt_pb.py exam_tt_pb.py ; cp ${ROOT_PATH}/toulbar2/measure.py measure.py ; cp ${ROOT_PATH}/toulbar2/result.py result.py ; cp ${ROOT_PATH}/toulbar2/compare.py compare.py ; python exam_tt_main.py $VERSION_NAME

cd ..

###############################################################################

JEU=D2-1-18 
JEU=D2-2-18 
JEU=D2-3-18 
JEU=D3-2-16 
JEU=D3-2-17 
JEU=D3-2-18 
JEU=D3-3-16 
JEU=D3-3-17 
JEU=D3-3-18 
JEU=D5-3-18 
JEU=toy

VERSION_NAME=PRPRPR_3

mkdir ${JEU}_${VERSION_NAME}
cd ${JEU}_${VERSION_NAME}

echo "$JEU" > JEU.txt ; rm -f data.py ; rm -f dataJSON.py ; rm -f exam_tt_main.py ; rm -f exam_tt_pb.py ; rm -f measure.py ; rm -f result.py ; rm -f compare.py ; ln -s ${DATA_PY_PATH}/${JEU}.py data.py ; ln -s ${DATA_JSONPY_PATH}/${JEU}JSON.py dataJSON.py ; cp ${ROOT_PATH}/toulbar2/exam_tt_main.py exam_tt_main.py ; cp ${ROOT_PATH}/toulbar2/exam_tt_pb.py exam_tt_pb.py ; cp ${ROOT_PATH}/toulbar2/measure.py measure.py ; cp ${ROOT_PATH}/toulbar2/result.py result.py ; cp ${ROOT_PATH}/toulbar2/compare.py compare.py ; python exam_tt_main.py $VERSION_NAME > STDOUT.txt

cd ..

