###############################################################################
# 
#                     ws run of exam_tt_main.py

#
###############################################################################

source SETTINGS.sh
TOULBAR2_PATH=${ROOT_PATH}/toulbar2
TOULBAR2_WS_PATH=${TOULBAR2_PATH}/ws

#------------------------------------------------------------------------------
Creation :

mkdir input
cp ${TOULBAR2_PATH}/exam_tt_main.py input/.
cp ${TOULBAR2_PATH}/exam_tt_pb.py input/.
cp ${TOULBAR2_PATH}/result.py input/.
cp ${TOULBAR2_PATH}/measure.py input/.
cp ${TOULBAR2_PATH}/compare.py input/.
cp ${TOULBAR2_WS_PATH}/exam_tt_cmd.sh input/.

#------------------------------------------------------------------------------
# ws run ou muse/run :
# 1. construire input.zip
# 2. requete run ou muse/run

BASE_WS_URL=http://147.100.179.250

TOKENVALUE=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6Indzc3UiLCJleHAiOjE2MzQ2MDMxMDcsImVtYWlsIjoiIn0.dAiiI1M5Vg_HQ924LQUIso5jgKjOXu_2IQJEdlEihRY

echo "DATA_PY_PATH = $DATA_PY_PATH"
echo "DATA_JSONPY_PATH = $DATA_JSONPY_PATH"
echo "BASE_WS_URL = $BASE_WS_URL"
echo "JEUX_SEL = $JEUX_SEL"

Choose VERSION_NAME in `echo $VERSION_NAMES` :
VERSION_NAME=EEE
VERSION_NAME=PRPRPR_4
VERSION_NAME=PRPRPR_3

for JEU in `echo $JEUX_SEL`
do
    echo "$JEU" > input/JEU.txt ;
    rm -f input/JEU_brut_result.txt; rm -f input/JEU_result_as_pr.txt; touch input/JEU_brut_result.txt; touch input/JEU_result_as_pr.txt;
    rm -f input.zip ; rm -f input/data.py ; rm -f input/dataJSON.py ; cp ${DATA_PY_PATH}/${JEU}.py input/data.py ; cp ${DATA_JSONPY_PATH}/${JEU}JSON.py input/dataJSON.py ; zip -r input.zip input
    curl --output _ws_${JEU}_run.zip -F 'jwt='${TOKENVALUE} -F 'file=@input.zip' -F 'cmd=/bin/bash input/exam_tt_cmd.sh '${VERSION_NAME} -F 'returned_type=run.zip' -F 'todownload=no' ${BASE_WS_URL}/api/software/run/pytoulbar2/
done

ou

echo $JEUX_SEL
for JEU in `echo $JEUX_SEL`
do
    echo "$JEU" > input/JEU.txt ;
    rm -f input/JEU_brut_result.txt; rm -f input/JEU_result_as_pr.txt; touch input/JEU_brut_result.txt; touch input/JEU_result_as_pr.txt;
    rm -f input.zip ; rm -f input/data.py ; rm -f input/dataJSON.py ; cp ${DATA_PY_PATH}/${JEU}.py input/data.py ; cp ${DATA_JSONPY_PATH}/${JEU}JSON.py input/dataJSON.py ; zip -r input.zip input
    curl --output _ws_${JEU}_muse_run.txt -F 'jwt='${TOKENVALUE} -F 'file=@input.zip' -F 'cmd=/bin/bash input/exam_tt_cmd.sh '${VERSION_NAME} -F 'returned_type=run.zip' -F 'todownload=no' ${BASE_WS_URL}/api/software/muse/run/pytoulbar2/

done

#------------------------------------------------------------------------------

