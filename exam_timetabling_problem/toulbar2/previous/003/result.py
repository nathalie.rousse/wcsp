
from dataJSON import dataJSON 

def brut_result(res) :
    """return brut_result from res : Problem.Solve() result """

    (s, v1, v2) = res

    try :
        fichier = open("JEU_brut_result.txt", "a")
        fichier.write(str(s))
        fichier.close()
    except :
        pass

    print("\n", "------------", "Solution brute :", "\n")
    print(res)
    print("s_brut= ", s)

    return s


def pr_result(s, version_name, Events, Periods, Rooms) :

    PeriodsRooms = Periods * Rooms

    s_pr = list()

    if version_name=="EEE":

        def get_PR_value(value) :
            if value > PeriodsRooms :
                return None
            i = 0
            for p in range(Periods) :
                for r in range(Rooms) :
                    if i == value :
                        return (p, r)
                    i = i + 1

        for value in s :
            (p,r) = get_PR_value(value)
            s_pr.append(p)
            s_pr.append(r)

    elif version_name=="PRPRPR_4" :
        for v in s :
            s_pr.append(v)

    elif version_name=="PRPRPR_3" :
        i = 0
        for e in range(Events) :
            period = s[i]
            s_pr.append(period)
            i += 1
            room = s[i]
            s_pr.append(room)
            i += 1

    else :
        for v in s :
            s_pr.append(v)

    try :
        fichier = open("JEU_result_as_pr.txt", "a")
        fichier.write(str(s_pr))
        fichier.close()
    except :
        pass

    print("\n", "------------", "ie as [Period, Room, Period, Room, ...] :", "\n")
    print("s_as_pr= ", s_pr)
    return s_pr

def extract_results(s, Rooms) :
    """presents detailed results from s (s_pr format) """

    # rooms_ident 
    rooms_ident = list()
    RoomsJSON = dataJSON["Rooms"]
    RoomsJSON.append({'Room': 'dummy', 'Type': 'dummy'})
    if Rooms == len(RoomsJSON) :
        for r in RoomsJSON :
            e = r['Room']
            abr = { "Small":"S", "Medium":"M", "Large":"L"}
            t = r['Type']
            if t in abr.keys() :
                e = e + " [" +abr[t]+ "]"
            rooms_ident.append(e)
    else :        
        rooms_ident = [r for r in range(Rooms)] # default (unknown)

    # solution, chrono_solution
    solution = list()
    i = 0
    event = 0
    while i < len(s) :
        period = s[i]
        i += 1
        room = s[i]
        room_ident = rooms_ident[ room ]
        i += 1
        solution.append( (period,room, event, room_ident) )
        event += 1
    chrono_solution = sorted(solution)

    print("\n", "------------", "Info rooms :", "\n")
    for (i,r) in enumerate(rooms_ident) :
        print(i, " : ", r)
    print("\n", "------------", "Par event croissant :", "\n")
    flegend = '{0:>8s} {1:>8s} {2:>8s}     {3:20s}'
    print(flegend.format("event", "period", "room", "room ident"))
    f = '{0:8d} {1:8d} {2:8d}     {3:20s}'
    for e in solution :
        (period,room, event, room_ident) = e
        print(f.format(event, period, room, room_ident))
    print("\n", "------------", "Dans l'ordre chronologique (period croissant) :", "\n")
    print(flegend.format("event", "period", "room", "room ident"))
    for e in chrono_solution :
        (period,room, event, room_ident) = e
        print(f.format(event, period, room, room_ident))
    print("\n", "------------")

