# Calling toulbar2
  
B. Different ways to call toulbar2
==================================
- Call by C++ code
- Call by python code
- Call by http requests/responses

B.a. Call by C++ code
=====================

...

B.b. Call by python code
========================

- To be able to call python code :

  ln -s {_path_to_}/pytoulbar2.cpython-37m-x86_64-linux-gnu.so pytoulbar2.cpython-37m-x86_64-linux-gnu.so

  ln -s {_path_to_}/CFN.py CFN.py

- How :

  - write a python code using pytoulbar2 to run toulbar2.

- call example :

  python3 airland.py airland2.txt

B.c. Call by http requests/responses
====================================

...

- How :

  - Have a code to run toulbar2 : a C++ code or a python code using pytoulbar2.
  - write and send a http request calling the web services to run this code,
    then receive the web services http response.
    send/receive http request/response : in curl, a web browser, any 
    programming language supporting http protocol.

_todo_ ws_deliv/toulbar2,pytoulbar2 :
     - exemples d'appels
     - scripts python specifiques ws_run_pytoulbar2.py

