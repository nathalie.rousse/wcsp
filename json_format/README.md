
JSON format
===========

- JSON Schema :

  Using JSON Schema to validate JSON file structure/format/syntax.

  JSON Schema is a vocabulary allowing to annotate and validate JSON documents.

  URLs :
  - json-schema.org
  - json-schema.org/understanding-json-schema

Some tools
==========

- jq

  jq is a lightweight and flexible command-line JSON processor.

  URL : https://stedolan.github.io/jq

  Install : sudo apt install jq

  Use examples :

      jq '.problem' files/latin4.json

      jq '.functions' files/latin4.json

- JSON format Validation/Visualisation 

  jsonlint.com

  jsonviewer.stack.hu

- JSON Schema Validator

  To validate schema syntax : jsonschemavalidator.net

- Voir https://geekflare.com/fr/json-online-tools
  jsoncompare.com
  jsonformatter.org
  onlinejsontools.com
  jsoneditoronline.org
  jsonformatter.io
  jsonformatter-online.com
  json.parser.online.fr
  site24x7.com/tools/json-generator.html
  etc

- JSON Generator

  json-generator.com

  objgen.com/json

  extendsclass.com/json-generator.html

toulbar2 JSON files
===================

JSON format used to define/describe some toulbar2 problems : cfn files...

Into 'files' folder : some files from toulbar2.

Found : some .cfn files contents with syntax errors :

  -  problem   variables   functions   scope instead of
    "problem" "variables" "functions" "scope"

  - into "costs" : sometimes varname instead of "varname"

  - some functions dict values without any key.

Example :

  See differences between original files/latin4.cfn and modified
  files/latin4.json
  
RapidJSON
=========

RapidJSON is a fast JSON parser/generator for C++ with both SAX/DOM style API.

URLs :
- rapidjson.org
- rapidjson.org/md_doc_schema.html

For more see : json_schema/rapidjson

Some tests : json_schema/rapidjson

simdjson
========

Parsing gigabytes of JSON per second

JSON is everywhere on the Internet. Servers spend a lot of time parsing it.
The simdjson library uses commonly available SIMD instructions and
microparallel algorithms to break speed records.

URLs :
- https://simdjson.org/
- https://github.com/simdjson/simdjson

https://medium.com/@rajat_sriv/parsing-gigabytes-of-json-per-second-9c5a1a7b91db

https://www.teluq.ca/siteweb/univ/dlemire.html

