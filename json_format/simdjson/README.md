# simdjson

Description
===========

simdjson : Parsing gigabytes of JSON per second

Install
=======

git clone https://github.com/simdjson/simdjson.git

The simdjson.cpp and simdjson.h files copied into src, into quickstart are
coming from the resulting simdjson.

Some tests
==========

quickstart
----------

- Code : quickstart/quickstart*.cpp

- Build :

    cd quickstart ;
    /usr/bin/c++ -o quickstart quickstart.cpp simdjson.cpp
    /usr/bin/c++ -o quickstart2 quickstart2.cpp simdjson.cpp
    /usr/bin/c++ -o quickstart_noexceptions quickstart_noexceptions.cpp simdjson.cpp
    /usr/bin/c++ -o quickstart2_noexceptions quickstart2_noexceptions.cpp simdjson.cpp

- Run :

    ./quickstart
    ./quickstart2
    ./quickstart_noexceptions
    ./quickstart2_noexceptions

examples
--------

- Code : src/examples.cpp

- Build :

    cd src ;
    /usr/bin/c++ -o examples examples.cpp simdjson.cpp


- Run :

    ./examples

main
----

- Code : src/main.cpp

- Fixed JSON input file : files/myfile.json

- Build :

    cd src ;
    /usr/bin/c++ -o main main.cpp simdjson.cpp

- Run :

    ./main

