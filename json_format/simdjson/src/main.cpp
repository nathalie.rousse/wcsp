#include "simdjson.h"

int main(void) {

  std::cout << "JSON file : files/myfile.json" << std::endl << std::endl;

  simdjson::dom::parser parser;
  simdjson::dom::element cfn = parser.load("files/myfile.json");

  std::cout << "\n**** Problem :" << std::endl;
  simdjson::dom::object problem = cfn["problem"];
  std::cout << std::endl;
  std::cout << "- Problem name : " << problem["name"];
  std::cout << "    (type : " << problem["name"].type() << ")" << std::endl;
  std::cout << "- Problem mustbe : " << problem["mustbe"];
  std::cout << "    (type : " << problem["mustbe"].type() << ")" << std::endl;

  std::cout << "\n**** Variables :" << std::endl;
  simdjson::dom::object variables = cfn["variables"];
  for(auto kv : variables) {
      std::cout << std::endl;
      std::cout << "- Variable name : " << kv.key << std::endl;
      std::cout << std::endl;
      std::cout << "  Variable domain : " << kv.value; 
      std::cout << "    (type : " << kv.value.type() << ")" << std::endl;
      std::cout << std::endl;
      std::cout << "    List of domain values :" << std::endl;
      for (auto v : kv.value) {
          std::cout << "    " << v;
          std::cout << "    (type : " <<  v.type() << ")" << std::endl;
      }
  }

  std::cout << "\n**** Functions :" << std::endl;
  simdjson::dom::object functions = cfn["functions"];
  for(auto kv : functions) {
      std::cout << std::endl;
      std::cout << "- Function name : " << kv.key << std::endl;
      simdjson::dom::object fn = kv.value;
      for(auto fn_kv : fn) {
          if (fn_kv.key == "scope"){
              std::cout << std::endl;
              std::cout << "  scope : " << fn_kv.value;
              std::cout << "    (type : " <<  fn_kv.value.type() << ")";
              std::cout << std::endl;
              std::cout << std::endl;
              std::cout << "    List of scope values :" << std::endl;
              for (auto v : fn_kv.value) {
                  std::cout << "    " << v;
                  std::cout << "    (type : " <<  v.type() << ")" << std::endl;
              }
          } else if (fn_kv.key == "defaultcost"){
              std::cout << std::endl;
              std::cout << "  defaultcost : " << fn_kv.value;
              std::cout << "    (type : " <<  fn_kv.value.type() << ")";
              std::cout << std::endl;
          } else if (fn_kv.key == "costs"){
              std::cout << std::endl;
              std::cout << "  costs : " << fn_kv.value; 
              std::cout << "    (type : " <<  fn_kv.value.type() << ")";
              std::cout << std::endl;
              std::cout << std::endl;
              std::cout << "    List of costs values :" << std::endl;
              for (auto v : fn_kv.value) {
                  std::cout << "    " << v;
                  std::cout << "    (type : " <<  v.type() << ")" << std::endl;
              }
          } else {
              std::cout << std::endl;
              std::cout << "CLE INCONNUE" << std::endl;
          }
      }
  }
}

