#include "simdjson.h"

int main(void) {
  /*
  simdjson::dom::parser parser;
  simdjson::dom::element tweets = parser.load("twitter.json");
  std::cout << tweets["search_metadata"]["count"] << " results." << std::endl;
  */

// 1)
{
auto cars_json = R"( [
  { "make": "Toyota", "model": "Camry",  "year": 2018, "tire_pressure": [ 40.1, 39.9, 37.7, 40.4 ] },
  { "make": "Kia",    "model": "Soul",   "year": 2012, "tire_pressure": [ 30.1, 31.0, 28.6, 28.7 ] },
  { "make": "Toyota", "model": "Tercel", "year": 1999, "tire_pressure": [ 29.8, 30.0, 30.2, 30.5 ] }
] )"_padded;
simdjson::dom::parser parser;

// Iterating through an array of objects
for (simdjson::dom::object car : parser.parse(cars_json)) {
  // Accessing a field by name
  std::cout << "Make/Model: " << car["make"] << "/" << car["model"] << std::endl;

  // Casting a JSON element to an integer
  uint64_t year = car["year"];
  std::cout << "- This car is " << 2020 - year << "years old." << std::endl;

  // Iterating through an array of floats
  double total_tire_pressure = 0;
  for (double tire_pressure : car["tire_pressure"]) {
    total_tire_pressure += tire_pressure;
  }
  std::cout << "- Average tire pressure: " << (total_tire_pressure / 4) << std::endl;

  // Writing out all the information about the car
  for (auto field : car) {
    std::cout << "- " << field.key << ": " << field.value << std::endl;
  }
}
}
// 2)
{
auto abstract_json = R"( [
    {  "12345" : {"a":12.34, "b":56.78, "c": 9998877}   },
    {  "12545" : {"a":11.44, "b":12.78, "c": 11111111}  }
  ] )"_padded;
simdjson::dom::parser parser;

// Parse and iterate through an array of objects
for (simdjson::dom::object obj : parser.parse(abstract_json)) {
    for(const auto& key_value : obj) {
      std::cout << "key: " << key_value.key << " : ";
      simdjson::dom::object innerobj = key_value.value;
      std::cout << "a: " << double(innerobj["a"]) << ", ";
      std::cout << "b: " << double(innerobj["b"]) << ", ";
      std::cout << "c: " << int64_t(innerobj["c"]) << std::endl;
    }
}
}
// 3)
{
auto abstract_json = R"(
    {  "str" : { "123" : {"abc" : 3.14 } } } )"_padded;
  simdjson::dom::parser parser;
  double v = parser.parse(abstract_json)["str"]["123"]["abc"];
  std::cout << "number: " << v << std::endl;
}

}
