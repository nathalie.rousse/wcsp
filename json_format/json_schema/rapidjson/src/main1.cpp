#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "rapidjson/error/en.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/schema.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"

//using namespace rapidjson;


int main (int argc, char *argv[]) {  

    //std::cout << "DEBUT main1 ---------------------------------" << "\n" ;

    // inputs
    const char* schema_json_filepath = "../files/myschema.json";
    const char* file_json_filepath = "../files/myfile.json";

    // Read JSON schema from file --> Document
    rapidjson::Document sd;
    {
        char schbuffer[4096]; // size !!!
        FILE *fp = fopen(schema_json_filepath, "r");
        if (!fp) {
            printf("Schema file '%s' not found\n", schema_json_filepath);
            return -1;
        }

        rapidjson::FileReadStream fs(fp, schbuffer, sizeof(schbuffer));

        //std::cout << "------------------------------" << "\n" ;
        //std::cout << "JSON SCHEMA : " << "\n" << schbuffer;
        //std::cout << "------------------------------" << "\n" ;

        sd.ParseStream(fs);
        if (sd.HasParseError()) {
            fprintf(stderr, "Schema file '%s' is not a valid JSON\n",
                    schema_json_filepath);
            fprintf(stderr, "Error(offset %u): %s\n",
                    static_cast<unsigned>(sd.GetErrorOffset()),
                    GetParseError_En(sd.GetParseError()));
            fclose(fp);
            return EXIT_FAILURE;
        }
        fclose(fp);
    }

    // Convert Document into SchemaDocument
    rapidjson::SchemaDocument schemadoc(sd);

    // Read JSON file from file --> Document
    rapidjson::Document doc;
    {
        char buffer[1000000]; // size !!!!!!!!
        FILE *fp = fopen(file_json_filepath, "r");
        if (!fp) {
            printf("JSON file '%s' not found\n", file_json_filepath);
            return -1;
        }

        rapidjson::FileReadStream fs(fp, buffer, sizeof(buffer));

        //std::cout << "------------------------------" << "\n" ;
        //std::cout << "JSON FILE : " << "\n" << buffer;
        //std::cout << "------------------------------" << "\n" ;

        doc.ParseStream(fs);
        if (doc.HasParseError()) {
            fprintf(stderr, "JSON file '%s' is not a valid JSON\n",
                    file_json_filepath);
            fprintf(stderr, "Error(offset %u): %s\n",
                    static_cast<unsigned>(doc.GetErrorOffset()),
                    GetParseError_En(doc.GetParseError()));
            fclose(fp);
            return EXIT_FAILURE;
        }
        fclose(fp);
    }

    // Validation

    rapidjson::SchemaValidator validator(schemadoc);
    if (!doc.Accept(validator)) { // invalid

        // Output diagnostic information
        rapidjson::StringBuffer sb;
        validator.GetInvalidSchemaPointer().StringifyUriFragment(sb);
        printf("Invalid schema: %s\n", sb.GetString());
        printf("Invalid keyword: %s\n", validator.GetInvalidSchemaKeyword());
        sb.Clear();
        validator.GetInvalidDocumentPointer().StringifyUriFragment(sb);
        printf("Invalid document: %s\n", sb.GetString());

        // Detailed violation report as a JSON value
        sb.Clear();
        rapidjson::PrettyWriter<rapidjson::StringBuffer> w(sb);
        validator.GetError().Accept(w);
        fprintf(stderr, "Error report:\n%s\n", sb.GetString());

        return EXIT_FAILURE;

    } else {
        printf("\n VALID document \n");
    }

    //std::cout << "..FIN main1 ---------------------------------" << "\n" ;
    return 0;
}

