#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "rapidjson/error/en.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/schema.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"

//using namespace rapidjson;


int main (int argc, char *argv[]) {  

    //std::cout << "DEBUT main2 ---------------------------------" << "\n" ;

    /* inputs
    const char* schema_json_filepath = "../files/myschema.json";
    const char* file_json_filepath = "../files/myfile.json"; */

    if (argc != 2) {
        fprintf(stderr, "Usage: ./main2 schema.json < file.json \n");
        return EXIT_FAILURE;
    }

    // Read a JSON schema from file --> Document
    rapidjson::Document d;
    char buffer[4096];

    {
        FILE *fp = fopen(argv[1], "r");
        if (!fp) {
            printf("Schema file '%s' not found\n", argv[1]);
            return -1;
        }
        rapidjson::FileReadStream fs(fp, buffer, sizeof(buffer));
        d.ParseStream(fs);
        if (d.HasParseError()) {
            fprintf(stderr, "Schema file '%s' is not a valid JSON\n", argv[1]);
            fprintf(stderr, "Error(offset %u): %s\n",
                static_cast<unsigned>(d.GetErrorOffset()),
                rapidjson::GetParseError_En(d.GetParseError()));
            fclose(fp);
            return EXIT_FAILURE;
        }
        fclose(fp);
    }

    // Converts Document --> SchemaDocument
    rapidjson::SchemaDocument sd(d);

    // Use reader to parse the JSON from stdin,
    // and forward SAX events to validator
    rapidjson::SchemaValidator validator(sd);
    rapidjson::Reader reader;
    rapidjson::FileReadStream is(stdin, buffer, sizeof(buffer));
    if (!reader.Parse(is, validator) && reader.GetParseErrorCode() != rapidjson::kParseErrorTermination) {
        // Schema validator error would cause kParseErrorTermination,
        // which will handle it in next step.
        fprintf(stderr, "Input is not a valid JSON\n");
        fprintf(stderr, "Error(offset %u): %s\n",
            static_cast<unsigned>(reader.GetErrorOffset()),
            rapidjson::GetParseError_En(reader.GetParseErrorCode()));
        return EXIT_FAILURE;
    }

    // Check the validation result
    if (validator.IsValid()) {
        printf("Input JSON is valid.\n");
        return EXIT_SUCCESS;
    }
    else {
        printf("Input JSON is invalid.\n");
        rapidjson::StringBuffer sb;
        validator.GetInvalidSchemaPointer().StringifyUriFragment(sb);
        fprintf(stderr, "Invalid schema: %s\n", sb.GetString());
        fprintf(stderr, "Invalid keyword: %s\n",
                validator.GetInvalidSchemaKeyword());
        sb.Clear();
        validator.GetInvalidDocumentPointer().StringifyUriFragment(sb);
        fprintf(stderr, "Invalid document: %s\n", sb.GetString());

        // Detailed violation report as a JSON value
        sb.Clear();
        rapidjson::PrettyWriter<rapidjson::StringBuffer> w(sb);
        validator.GetError().Accept(w);
        fprintf(stderr, "Error report:\n%s\n", sb.GetString());

        return EXIT_FAILURE;
    }

    //std::cout << "..FIN main2 ---------------------------------" << "\n" ;
    return 0;
}

