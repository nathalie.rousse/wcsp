# RapidJSON

Description
===========

RapidJSON is a fast JSON parser/generator for C++ with both SAX/DOM style API.

URLs :
- rapidjson.org
- rapidjson.org/md_doc_schema.html

Et aussi :
- https://miloyip.gitbooks.io/rapidjson/content/en/doc/schema.html
- (code on the fly) : https://github.com/Tencent/rapidjson/blob/master/example/schemavalidator/schemavalidator.cpp

RapidJSON implemented a JSON Schema validator for JSON Schema Draft v4.

Validation during parsing/serialization : Unlike most JSON Schema validator
implementations, RapidJSON provides a SAX-based schema validator. Therefore,
you can parse a JSON from a stream while validating it on the fly. If the
validator encounters a JSON value that invalidates the supplied schema, the
parsing will be terminated immediately. This design is especially useful for
parsing large JSON files.

Install
=======

RapidJSON is a header-only C++ library. Just copy the include/rapidjson folder
to system or project's include path.

git clone https://github.com/Tencent/rapidjson.git ;
cp -fr rapidjson/include/rapidjson src/rapidjson


Some tests
==========

When parsing a JSON from file, you may read the whole JSON into memory and use
StringStream. However, if the JSON is big, or memory is limited, you can use
FileReadStream. It only reads a part of JSON from file into buffer, and then
let the part be parsed. If it runs out of characters in the buffer, it will
read the next part from file.

main1
-----

- Code : src/main1.cpp 

- Fixed inputs : ../files/myschema.json ../files/myfile.json

- Build :

    cd src ;
    /usr/bin/c++ -std=c++11 main1.cpp -o main1

- Run :

    ./main1

main2
-----

- Uses FileReadStream.

- Code : src/main2.cpp 

- Build :

    cd src ;
    /usr/bin/c++ -std=c++11 main2.cpp -o main2

- Run :

    ./main2 schema.json < file.json

    ./main2 ../files/myschema.json < ../files/myfile.json


- Cases :

    ./main2 ../files/schema_A.json < ../files/file_ok_A.json

    /main2 ../files/myschema.json < ../files/airland4.json

Input JSON is valid.

- Case :

    ./main2 ../files/schema_A.json < ../files/file_nok_A.json

Input JSON is invalid.
Invalid schema: #
Invalid keyword: patternProperties
Invalid document: #/variables/x0
Error report:
{
    "type": {
        "expected": [
            "string"
        ],
        "actual": "integer",
        "instanceRef": "#/variables/x0/0",
        "schemaRef": "#/properties/variables/patternProperties/%5Ex/items"
    }
}


