#include <iostream>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace rapidjson;

int main (int argc, char *argv[]) {  

    //std::cout << "DEBUT main2 ---------------------------------" << "\n" ;

	rapidjson::Document document;
	document.SetObject();
	rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

	// problem
	rapidjson::Value object_problem(rapidjson::kObjectType);
	object_problem.AddMember("name", "problem", allocator);
	object_problem.AddMember("mustbe", "<99999", allocator);
	document.AddMember("problem", object_problem, allocator);

	// variables
	rapidjson::Value object_variables(rapidjson::kObjectType);

	rapidjson::Value array_x0(rapidjson::kArrayType);
	array_x0.PushBack("v129", allocator).PushBack("v130", allocator).PushBack("v131", allocator);
	object_variables.AddMember("x0", array_x0, allocator);

	rapidjson::Value array_x1(rapidjson::kArrayType);
        array_x1.PushBack("v190", allocator).PushBack("v191", allocator).PushBack("v192", allocator).PushBack("v193", allocator).PushBack("v194", allocator);
	object_variables.AddMember("x1", array_x1, allocator);

	document.AddMember("variables", object_variables, allocator);

        // functions
        rapidjson::Value object_functions(rapidjson::kObjectType);

        {
            // f
            rapidjson::Value f(rapidjson::kObjectType);

            // scope
            rapidjson::Value scope(rapidjson::kArrayType);
            scope.PushBack("x0",allocator).PushBack("x1",allocator);
            f.AddMember("scope", scope, allocator);

            // defaultcost
	    f.AddMember("defaultcost", 0, allocator);

            // costs
            rapidjson::Value costs(rapidjson::kArrayType);
            costs.PushBack(59,allocator).PushBack(0,allocator).PushBack(99999,allocator).PushBack(60,allocator).PushBack(0,allocator).PushBack(99999,allocator).PushBack(60,allocator).PushBack(1,allocator).PushBack(99999,allocator).PushBack( 430,allocator).PushBack(371,allocator).PushBack(99999,allocator);
            f.AddMember("costs", costs, allocator);
	    object_functions.AddMember("F_0_1", f, allocator);
        }
        {
            rapidjson::Value f(rapidjson::kObjectType);
            rapidjson::Value scope(rapidjson::kArrayType);
            scope.PushBack("x0",allocator).PushBack("x2",allocator);
            f.AddMember("scope", scope, allocator);
	    f.AddMember("defaultcost", 0, allocator);
            rapidjson::Value costs(rapidjson::kArrayType);
            costs.PushBack(0,allocator).PushBack(31,allocator).PushBack(99999,allocator).PushBack( 0,allocator).PushBack(32,allocator).PushBack(99999,allocator).PushBack( 385,allocator).PushBack(417,allocator).PushBack(99999,allocator).PushBack( 386,allocator).PushBack(417,allocator).PushBack(99999,allocator);
            f.AddMember("costs", costs, allocator);
	    object_functions.AddMember("F_0_2", f, allocator);
        }
        {
            rapidjson::Value f(rapidjson::kObjectType);
            rapidjson::Value scope(rapidjson::kArrayType);
            scope.PushBack("x0",allocator).PushBack("x3",allocator);
            f.AddMember("scope", scope, allocator);
	    f.AddMember("defaultcost", 0, allocator);
            rapidjson::Value costs(rapidjson::kArrayType);
            costs.PushBack(0,allocator).PushBack(26,allocator).PushBack(99999,allocator).PushBack(0,allocator).PushBack(27,allocator).PushBack(99999,allocator).PushBack(0,allocator).PushBack(28,allocator).PushBack(99999,allocator).PushBack(393,allocator).PushBack(420,allocator).PushBack(99999,allocator).PushBack(394,allocator).PushBack(420,allocator).PushBack(99999,allocator);
            f.AddMember("costs", costs, allocator);
	    object_functions.AddMember("F_0_3", f, allocator);
        }
        {
            rapidjson::Value f(rapidjson::kObjectType);
            rapidjson::Value scope(rapidjson::kArrayType);
            scope.PushBack("x0",allocator).PushBack("x4",allocator);
            f.AddMember("scope", scope, allocator);
	    f.AddMember("defaultcost", 0, allocator);
            rapidjson::Value costs(rapidjson::kArrayType);
            costs.PushBack(0,allocator).PushBack(15,allocator).PushBack(99999,allocator).PushBack(0,allocator).PushBack(16,allocator).PushBack(99999,allocator).PushBack(420,allocator).PushBack(436,allocator).PushBack(99999,allocator).PushBack(421,allocator).PushBack(436,allocator).PushBack(99999,allocator);
            f.AddMember("costs", costs, allocator);
	    object_functions.AddMember("F_0_4", f, allocator);
        }
        {
            rapidjson::Value f(rapidjson::kObjectType);
            rapidjson::Value scope(rapidjson::kArrayType);
            scope.PushBack("x0",allocator);
            f.AddMember("scope", scope, allocator);
	    f.AddMember("defaultcost", 99999, allocator);
            rapidjson::Value costs(rapidjson::kArrayType);
            costs.PushBack(0,allocator).PushBack(0,allocator).PushBack(260,allocator).PushBack(1,allocator).PushBack(430,allocator).PushBack(4040,allocator);
            f.AddMember("costs", costs, allocator);
	    object_functions.AddMember("F_0", f, allocator);
        }
        {
            rapidjson::Value f(rapidjson::kObjectType);
            rapidjson::Value scope(rapidjson::kArrayType);
            scope.PushBack("x1",allocator);
            f.AddMember("scope", scope, allocator);
	    f.AddMember("defaultcost", 99999, allocator);
            rapidjson::Value costs(rapidjson::kArrayType);
            costs.PushBack(0,allocator).PushBack(600,allocator).PushBack(1,allocator).PushBack(590,allocator).PushBack(4810,allocator).PushBack(542,allocator).PushBack(4820,allocator);
            f.AddMember("costs", costs, allocator);
	    object_functions.AddMember("F_1", f, allocator);
        }
        {
            rapidjson::Value f(rapidjson::kObjectType);
            rapidjson::Value scope(rapidjson::kArrayType);
            scope.PushBack("x14",allocator);
            f.AddMember("scope", scope, allocator);
	    f.AddMember("defaultcost", 99999, allocator);
            rapidjson::Value costs(rapidjson::kArrayType);
            costs.PushBack(0,allocator).PushBack(660,allocator).PushBack(1,allocator).PushBack(650,allocator).PushBack(2,allocator).PushBack(539,allocator).PushBack(4730,allocator);
            f.AddMember("costs", costs, allocator);
	    object_functions.AddMember("F_14", f, allocator);
        }

	document.AddMember("functions", object_functions, allocator);

	StringBuffer strbuf;
	Writer<StringBuffer> writer(strbuf);
	document.Accept(writer);

	std::cout << strbuf.GetString() << std::endl;

    //std::cout << "..FIN main2 ---------------------------------" << "\n" ;
    return 0;

}

