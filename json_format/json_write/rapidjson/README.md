# RapidJSON

Description
===========

Voir json_format/json_schema/rapidjson/README.md

Using RapidJSON as fast JSON generator for C++.

Install
=======

RapidJSON is a header-only C++ library. Just copy the include/rapidjson folder
to system or project's include path.

git clone https://github.com/Tencent/rapidjson.git ;
cp -fr rapidjson/include/rapidjson src/rapidjson

Some tests
==========

main1
-----

- Code : src/main1.cpp 

- Build :

    cd src ;
    /usr/bin/c++ -std=c++11 main1.cpp -o main1

- Run :

    ./main1


main2
-----

- Code : src/main2.cpp 

- Build :

    cd src ;
    /usr/bin/c++ -std=c++11 main2.cpp -o main2

- Run :

    ./main2

