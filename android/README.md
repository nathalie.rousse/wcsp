# Développement d'applications pour smartphone (Android)

  Table des matieres

  - [A. Introduction](#A)
  - [B. Android https://developer.android.com](#B)
  - [C. Des packages de Machine Learning pour applications Android en Java/Kotlin](#C)
  - [D. Python and Android](#D)
  - [E. Applications Stores](#E)
  - [F. Sudoku applications on smartphone](#F)

# <a name="A"></a> A. Intro

- Android Developer Fundamentals 
  https://google-developer-training.github.io/android-developer-fundamentals-course-concepts-v2

- Langages de programmation :

  Les langages de programmation d'applications Android les plus communs : 
  Java et Kotlin, puis C/C++, puis C#.

  Des sources :

  - "Quel langage pour developper une application mobile ?"
    https://fr.goodbarber.com/creer-app/71-quel-langage-pour-developper-une-application-mobile/

  - "Quel langage de programmation développement d’applications Android ?"
    https://blog.back4app.com/fr/quel-langage-de-programmation-est-utilise-pour-le-developpement-dapplications-android

  Présentation de **Kotlin** :
  https://openclassrooms.com/fr/courses/5353106-initiez-vous-a-kotlin/5353113-decouvrez-l-histoire-de-kotlin

# <a name="B"></a> B. Android  https://developer.android.com

## Plaftorm Android :

   https://developer.android.com

   The Android Platform **API** (available in **Kotlin** and **Java**) :
   https://developer.android.com/reference

   Android API Levels : https://apilevels.com

       Android version  -->  SDK / API level

         Android 13          Level 33
         ...
         Android 10          Level 29
         ... 
         Android 5           Level 22, 21
         ... 

## Android Studio SDK (Software Development Kit) :

  https://developer.android.com/studio

  **Android Studio** is the official Integrated Development Environment
  (**IDE**) for Android app development.

  Langages : 
  - programmation Java/Kotlin et fichiers de configuration XML d'une application
    Android. 
  - C++ and NDK support.

  How :

  - Install Android Studio : https://developer.android.com/studio

  - Open Android Studio :

        cd ~/android-studio-2021.2.1.15-linux/android-studio/bin
        ./studio.sh

  - Video sur Android Studio : https://www.youtube.com/watch?v=e0fXuyL0xVU

  - Exemple tutoriel
    "Créer une application mobile sur Android avec Kotlin et XML" :
    https://www.youtube.com/watch?v=WlDzTh4WXek

## Android NDK (Native Development Kit) :

  https://developer.android.com/ndk

  The Android **NDK** is a toolset that lets you implement parts of your app
  in native code, using languages such as **C** and **C++**.

  For certain types of apps, this can help you reuse code libraries written in
  those languages. 

# <a name="C"></a> C. Des packages de Machine Learning pour applications Android en Java/Kotlin

  - **ML Kit** https://developers.google.com/ml-kit
  - **OpenCv4Android** https://opencv.org/android
  - **Tesseract4Android** https://github.com/adaptech-cz/Tesseract4Android
  - **TensorFlow Lite** https://www.tensorflow.org/lite
  - ...

## Exemples :

- **MLKit Samples** https://github.com/googlesamples/mlkit

  A collection of quickstart samples demonstrating the **ML Kit** APIs on
  Android and iOS.


- Dans les exemples de Android Studio :
  **TensorFlowLiteinPlayServicesimageclassificationAndroidexampleapplication**

  Utilise **Tensorflow Lite** in Play Services dependencies

  Dependencies :

      implementation 'org.jetbrains.kotlin:kotlin-stdlib-jdk8',
      implementation "androidx.camera:camera-...',
      implementation "com.google.android.gms:play-services-tflite-java:16.0.0",
      implementation "com.google.android.gms:play-services-tflite-support:16.0.0",
      implementation "com.google.android.gms:play-services-tflite-gpu:16.0.0",
      ...

- See [F. Sudoku applications on smartphone](#F)

- **OCR** https://github.com/SubhamTyagi/android-ocr

  An OCR app that can recognize texts on image.

  Get in on **F-DROID** App Store

  This App is based on **Tesseract 5**
  (https://github.com/tesseract-ocr/tesseract)
  and its is first app which is based on Tesseract 5.
  This app is made possible by a library **Tesseract4Android**
  (https://github.com/adaptech-cz/Tesseract4Android).

- See https://codelabs.developers.google.com/?cat=Android&product=android

## ML Kit (pour Java/Kotlin)

https://developers.google.com/ml-kit

**ML Kit** : package "Machine learning pour les développeurs d'applications
mobiles" de Google.

Fonctionnalités reposant sur les modèles de Machine Learning de Google,
proposées gratuitement.

Langages : appel en **Java/Kotlin**.

Les API de ML Kit s'exécutent toutes sur l'appareil :

- => permet traitement en temps réel (flux vidéo en direct...)
- => fonctionnalité disponible hors connexion.

## TensorFlow Lite

https://www.tensorflow.org/lite

**TensorFlow Lite** est un ensemble d'outils conçus pour aider les
développeurs à exécuter leurs modèles sur des appareils mobiles, intégrés et
IoT afin d'apporter le machine learning sur l'appareil.

## Tesseract4Android

https://github.com/adaptech-cz/Tesseract4Android

**Tesseract4Android** contains tools
for compiling the **Tesseract** and **Leptonica** libraries for use on the
Android platform. It provides a **Java API** (Java/JNI wrapper)
for accessing natively-compiled **Tesseract** and **Leptonica** **APIs**.

**Tesseract4Android** is a fork of **tess-two** rewritten from scratch to
build with **CMake** and support latest Android Studio and **Tesseract OCR**.

**tess-two** (https://github.com/rmtheis/tess-two) is a fork of 
'Tesseract Tools for Android' **tesseract-android-tools**
(https://github.com/alanv/tesseract-android-tools)
that adds some additional functions.

# <a name="D"></a> D. Python and Android

## Listes 

- Several ways to use **Python** on Android
  https://wiki.python.org/moin/Android :

  BeeWare, Chaquopy, Kivy, ... Termux.

  With more or less 'Native Python packages',
  with or without 'Call Python from Java'...

- See also 'Outils pour exécuter Python sur Android'
  https://fr.quish.tv/tools-run-python-android

- Plusieurs environnements, sur lesquels sont disponibles plus ou moins de
  librairies Python (des mises à jour plus ou moins récentes...).

## Kivy framework and python-for-android

Environnement Kivy utilisé pour developpement de 'Visual Sudoku' App.

- **Kivy** https://kivy.org : Open Source Python App development Framework. 
  
  - Bibliothèque libre et open source pour Python, de création d'applications
    tactiles pourvues d'une interface utilisateur naturelle.

  - Fonctionne sur Android, iOS, GNU/Linux, OS X et Windows.
    => 'Visual Sudoku' App disponible sous Linux.

  - GitHub : "Open source UI framework written in Python, running on Windows,
    Linux, macOS, Android and iOS" https://github.com/kivy/kivy

- En parallèle, Kivy développe **Python-for-Android** :

  - La plateforme Kivy a recours à **python-for-android** pour les librairies
    Python qui appellent C++.

  - https://python-for-android.readthedocs.io

  - **python-for-android** is an open source build tool to let you package Python
    code into standalone android APKs. This tool was originally developed for
    the Kivy framework, but now supports multiple bootstraps.

  - GitHub : "Turn your Python application into an Android APK"
    https://github.com/kivy/python-for-android

  - Existing recipes :
    https://github.com/kivy/python-for-android/tree/master/pythonforandroid/recipes

    Native Python packages : numpy, matplotlib, opencv, pandas...

  - Create your own recipe :
    https://python-for-android.readthedocs.io/en/latest/contribute/#create-your-own-recipes


- Trace essai pytoulbar2 (C++ library) avec Kivy :

  Dans buildozer.spec :

      requirements = python3,kivy,pytoulbar2

  Cmde : 

      buildozer -v android debug deploy run logcat | grep python
      =>
      /app/.buildozer/android/platform/build-arm64-v8a_armeabi-v7a/build/python-installs/solvesudoku/arm64-v8a/pytoulbar2 :
       __init__.py __init__.pyc
       pytb2.cpython-38-x86_64-linux-gnu.so*
       pytoulbar2.py pytoulbar2.pyc

      app/.buildozer/android/platform/build-arm64-v8a_armeabi-v7a/build/python-installs/solvesudoku/armeabi-v7a/pytoulbar2 :
       __init__.py __init__.pyc
       pytb2.cpython-38-x86_64-linux-gnu.so*
       pytoulbar2.py pytoulbar2.pyc

   Dans main.py :

       import pytoulbar2

  Cmde : 

       buildozer -v android debug deploy run logcat | grep python

       06-03 15:40:56.650 19656 19920 I python  :  ImportError: dlopen failed: "/data/user/0/org.test.solvesudoku/files/app/_python_bundle/site-packages/pytoulbar2/pytb2.so" is for EM_X86_64 (62) instead of EM_AARCH64 (183)

## Termux

**Termux** : a **Linux distribution** for Android that ships **Python**
as well as a local build environment.

## QPython3 and AIPY

- **QPython** https://www.qpython.org

  - **QPython** is a script engine that runs **Python** on android devices.
  - It lets your android device run Python scripts and projects.
  - It contains the Python interpreter, console, editor, and the SL4A Library
    for Android.

  - Un exemple QPython3 "Utiliser le GPS du téléphone ou tablette pour faire un
    suivi des positions dans un intervalle de temps" :
    https://continentcot.ca/blogue/2018/03/09/android-le-dompteur-de-python

- **AIPY** https://www.aipy.org :

  **AIPY** is a high-level AI learning app, based on related libraries like
  Numpy, Scipy, theano, keras, etc.... It was developed with a focus on
  helping you learn and practise AI programming well and fast.

  **AIPY** is released as a **QPython plugin** : after being installed, you
  can see the AIPY category in **QPYPI**, where you could find Numpy, Scipy,
  Pandas, Matplotlib, Scikit-learn, Theano, Lasange, Keras.

  By installing these **AIPY libraries**, you could start mathematics,
  scientific, data analytics, deeplearning etc. with **QPython**.

  https://edu.qpython.org/AIPY/index.html

- How :

       1. Install QPython.
       2. Install AIPY for QPython.
       3. QPython-->QPYPI-->AIPY install Numpy, SciPy, Matplotlib, openCV, etc
    
## Chaquopy

- **Chaquopy** : **Python** SDK for Android https://chaquo.com

- To use **Python** in an Android app :
  **Chaquopy** provides everything you need to include Python components in an
  Android app, including:

  - Full integration with **Android Studio**'s standard **Gradle** build system. 
  - Simple **APIs** for calling **Python** code from **Java/Kotlin**,
    and vice versa.
  - A wide range of third-party **Python packages**, including SciPy, OpenCV,
    TensorFlow etc.

- How to use Python With Android Studio | Chaquopy Tutorial :
  https://www.youtube.com/watch?v=1Qn24BOvZpA

## PyDroid3

- **Pydroid 3** : educational **Python3** **IDE** for Android.

- Contains :

  - Code editor + run
  - pip install
  - some '**Quick install**' for :
    tensorflow (payant), opencv-python (payant), torch (payant),
    PyQT5 cython jupyter keras ....

# <a name="E"></a> E. Applications Stores

**Google play Store**

- https://play.google.com/store
- https://fr.wikipedia.org/wiki/Google_Play

**F-Droid Store**

- https://f-droid.org
- F-Droid est un catalogue installable d’applications libres et à code source
  ouvert pour la plateforme Android. 
- https://fr.wikipedia.org/wiki/F-Droid
- Inclusion Policy : https://f-droid.org/fr/docs/Inclusion_Policy/

Quelques différences :
- voir "Indépendance" https://fr.wikipedia.org/wiki/F-Droid
- voir "Critiques" https://fr.wikipedia.org/wiki/Google_Play

Autres :
- APKPure (see https://fr.wikipedia.org/wiki/APKPure),
- Aptoide (see https://fr.wikipedia.org/wiki/Aptoide)


# <a name="F"></a> F. Sudoku applications on smartphone

- **Sudoku Solver App**

  https://github.com/hypertensiune/Android-Sudoku-Solver-OCR

  Android app for solving sudoku puzzles.

  Uses **OpenCv4Android** (https://opencv.org/android) for finding the
  location of the sudoku board on the image and
  **ML Kit** Vision Text Recognition
  (https://developers.google.com/ml-kit/vision/text-recognition/android) to
  retrieve the numbers.

  Dependencies :

      implementation 'androidx.camera:camera-...',
      implementation 'com.google.android.gms:play-services-mlkit-text-recognition:18.0.0' ...

- **Snap Solve Sudoku V2**

  https://github.com/Beebeeoii/snap-solve-sudoku

  Desc : 'Snap Solve Sudoku V2' is completely rewritten from 'V1'.
  Leveraging on Machine Learning using **Tensorflow** and personalised image
  processing algorithms with the use of **OpenCV**, 'Snap Solve Sudoku' aims
  to be accurate in detecting and recognising sudoku boards and its digits.
  Regardless of image source, be it from desktop monitors or printed media,
  the app's accuracy has been overwhelming!

  Technos : **Kotlin**, ***tensorflow-lite and Open-CV for Java/Kotlin***

  Dependencies :

      implementation project(path: ':openCVLibrary3410')
      implementation 'org.tensorflow:tensorflow-lite:2.2.0'
      implementation 'com.theartofdev.edmodo:android-image-cropper:2.8.0'

- **AndroidCameraSudokuSolver**

  https://github.com/jocstech/AndroidCameraSudokuSolver

  An OpenCV Android Camera Sudoku Solver

  Desc : a real-time sudoku game solver android application. The application
  will first scan the sudoku puzzle by the built-in camera. Then it will
  detect the game board by applying image edge detector. A sudoku game board
  contains 81 squares with number 1 - 9. The application need to identify the
  number and position of each square in the puzzle. After reading all numbers,
  it will calculate the solution and display it back to the image in real time.
  All solution numbers will attach to corresponding position of grid on the
  game board on screen even with slight movement of cellphone.

  Techno : **Java**, ***Tesseract and Open-CV for Java***

  Dependencies :

      compile 'com.rmtheis:tess-two:6.2.0'
      compile project(':openCVLibrary2410')

- **SudokuSolver**

  https://github.com/KhemnarMayuresh/SudokuSolver

  Desc : An android application that can solve a Sudoku puzzle by both manual
  and camera mode. The manual mode needs to enter numbers in each cell in
  the grid as in the puzzle. The in-camera mode device scans an image of
  Sudoku by using image processing and provide the solution for it.

  Technos : **Java**, Android **NDK** (jniLibs), **OpenCV version-2.4.10**,
  **Tesseract OCR**

  Dependencies :

      implementation 'com.rmtheis:tess-two:7.0.0'
      implementation project(path: ':openCVLibrary2410')

- **Sudoku-Solver**

  https://github.com/RivoLink/Sudoku-Solver

  An application for solving sudoku grid directly from android camera,
  using OpenCV for computer vision, and Tess-two for characters recognition.

  Techno : **Java**, ***Tesseract and Open-CV for Java***

  Dependencies :

      implementation 'org.opencv:openCVLibrary:3.4.0'
      implementation 'com.rmtheis:tess-two:9.0.0'

- **Sudoku Solver**

  https://github.com/samuelpratt/Sudoku/

  Desc : to take a picture of the puzzle, then the App will extract the
  puzzle from the image and then solve it.

  Techno : **Java**, ***Tesseract and Open-CV for Java***

  Needs : Android **NDK**, **OpenCV**, **Tess-Two**

  Dependencies :

      compile project(':openCVLibrary310-1')
      compile 'com.rmtheis:tess-two:6.1.1'

- **Sudoku Solver Android App**

  https://github.com/snehsagarajput/sudoku-solver-app

  Desc : An android app built with React-Native to solve any Sudoku Puzzle
  not only by entering the Sudoku Puzzle manually but also by just scanning
  the Sudoku Puzzle.

  Download the Android App or try the App on the Web using **Appetize**
  (https://appetize.io 'run mobile apps in your browser') :
  https://sudokusolver.pythonanywhere.com

  - Technos : **Java** + **Python**. *Architecture ?*

    Built With  : React-Native Tensorflow 2.0 Open-CV AWS Flask Gunicorn Nginx

- **Sudoku Camera Solver Android**

  https://github.com/yevtushenko520/SudokuCameraSolverAndroid

  Desc : An Android application that helps to solve Sudoku with a camera.

  Techno : **Java** + **C++** (JNI, **NDK**), **Open-CV**

  Native :

      externalNativeBuild {
          cmake {
              cppFlags "-frtti -fexceptions"
              abiFilters 'x86', 'armeabi-v7a', 'arm64-v8a'
              //arguments '-DANDROID_STL=c++_shared'
              arguments '-DANDROID_STL=gnustl_static'
          }
      }
      externalNativeBuild {
          cmake { path "CMakeLists.txt" }
      }

  Dependencies :

      implementation project(':opencv')

- **moulinet-sudoku-front-android**

  https://github.com/BenNG/moulinet-sudoku-front-android

  Techno : **Java** + **C++**, **Open-CV**

  Native :

      externalNativeBuild {
          cmake {
              cppFlags "-std=c++11 -frtti -fexceptions"
              abiFilters 'armeabi',  'armeabi-v7a',  'x86'
           }
      }
      externalNativeBuild {
          cmake { path "CMakeLists.txt" }
      }

  Dependencies :

      compile project(':openCVLibrary310')

- **sudoku-solver**

  https://github.com/AnimeAllstar/sudoku-solver

  Desc : An app to detect and solve sudokus using smartphone camera

  Technos : **Python**, **Kivy**, **Open-CV**, (**Tensorflow**?)

  buildozer.spec :

      requirements = python3,kivy,numpy,opencv

- **SudoCAM-Ku**

  https://github.com/Sanahm/SudoCAM-Ku

  Desc : An android camera-based augmented reality application that
  automatically solve and fill Sudoku puzzles by passing the camera over
  the puzzle.

  Technos : Android Studio,
  need to install **Tensorflow** and **OpenCV** in Android Studio (jniLibs)

  Based on a previous work **Sudoku-robot** :

    - https://github.com/Sanahm/Sudoku-robot
    - A robot that solves and fills alone a sudoku puzzles
    - Technos : in **Python**, **tensorflow** for neural network,
      **opencv** for image processing

Misc :

- **sudoku-scanner**

  https://github.com/taylorjg/sudoku-scanner

  Desc : A **web app** to scan and solve a Sudoku puzzle.
  Apps already exist for Android and iOS but I haven't seen this done as a
  web app before. The hard part is scanning the puzzle which is what I am
  attempting to do in this repo. 

  - Technos : **.js**, **TensorFlow.js**, **OpenCV.js**

  - The plan is to use **TensorFlow.js** to:

    - Train a model to find the bounding box of a Sudoku puzzle
    - Calculate the grid squares from the bounding box
    - Train a model to distinguish between blank grid squares and digits
    - Train a model to recognise digits 1-9

